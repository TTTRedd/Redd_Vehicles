

	class CfgVehicles {
		class LandVehicle;
		class StaticWeapon: LandVehicle {
			class Turrets {
				class MainTurret;
			};
		};

		class StaticMortar: StaticWeapon {
			class Turrets: Turrets {
				class MainTurret: MainTurret {
					class ViewOptics;
				};
			};
		};
		class Redd_Tank_M120_Tampella_Base: StaticMortar {

			picture="\A3\Static_f\Mortar_01\data\UI\Mortar_01_ca.paa";
			icon="\A3\Static_f\Mortar_01\data\UI\map_Mortar_01_CA.paa";
			side=1;
			crew = "B_Soldier_F";
			author = "Redd,Tank";
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Static";
			model="\Redd_Tank_M120_Tampella\Redd_Tank_M120_Tampella.p3d";
			gunnerCanSee = "31+32+14";
			availableForSupportTypes[]={"Artillery"};
			transportSoldier = 1;
			typicalCargo[] = {"B_Soldier_F"};
			memoryPointsGetInCargo = "pos cargo";
			memoryPointsGetInCargoDir = "pos cargo dir";
			cargoAction[] = {"Mortar_Gunner"};
			getInAction = "GetInMortar";
			getOutAction = "GetOutLow";
			unitInfoType = "RscUnitInfoMortar";
			cost = 200000;
			accuracy = 0.25;
			EPEImpulseDamageCoef = 5;
			artilleryScanner = 1;

			hiddenSelections[]={"moerser"};
			hiddenSelectionsTextures[]={"Redd_Tank_M120_Tampella\data\Redd_Tank_M120_Tampella_blend_co.paa"};

			class Damage {

				tex[]={};
				mat[]= {

					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser.rvmat",
					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser_damage.rvmat",
					"Redd_Tank_M120_Tampella\mats\Redd_Tank_M120_Tampella_Moerser_destruct.rvmat"
				};
			};

			class Turrets: Turrets {

				class MainTurret: MainTurret {
					
					gunnerAction = "Mortar_Gunner";
					elevationMode = 1;
					initCamElev = 0;
					minCamElev = -35;
					maxCamElev = 35;
					initElev = 0;
					minTurn = -180;
					maxTurn = 180;
					initTurn = 0;
					cameraDir = "look";
					discreteDistance[] = {100, 200, 300, 400, 500, 700, 1000, 1600, 2000, 2400, 2800};
					discreteDistanceCameraPoint[] = {"eye"};
					discreteDistanceInitIndex = 5;
					turretInfoType = "RscWeaponRangeArtillery";
					gunnerForceOptics = 0;
					memoryPointGunnerOptics = "eye";
					ejectDeadGunner = 1;
					usepip = 2;
					htMin = 1;
					htMax = 480;
					afMax = 0;
					mfMax = 0;
					mFact = 1;
					tBody = 100;
					gunnergetInAction="GetInMortar";
					getOutAction="GetOutLow";
					minelev=-27;
					maxelev=10;
					maxHorizontalRotSpeed = 0.35;
					maxVerticalRotSpeed = 0.35;
					hideWeaponsGunner = 0;
					memoryPointsGetInGunner = "pos gunner";
					memoryPointsGetInGunnerDir = "pos gunner dir";
					
					weapons[]= {

						"Redd_mortar_120mm"
					};

					magazines[]= {
						
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_annz_shells",
						"Redd_8Rnd_120mm_Mo_Flare_white",
						"Redd_8Rnd_120mm_Mo_Smoke_white"
					};

					class ViewOptics: ViewOptics {

						initFov=0.5;
						minFov=0.1;
						maxFov=0.5;

						visionMode[]= {

							"Normal"
						};
					};

					class OpticsIn {

						class Day1 {
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.75;
							maxFov = 0.75;
							minFov = 0.75;
							visionMode[] = {"Normal"};
							thermalMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_m120";
							gunnerOpticsEffect[] = {};		
						};
						
						class Day2 {
							
							initAngleX = 0;
							minAngleX = -30;
							maxAngleX = 30;
							initAngleY = 0;
							minAngleY = -100;
							maxAngleY = 100;
							initFov = 0.08;
							maxFov = 0.08;
							minFov = 0.08;
							visionMode[] = {"Normal"};
							thermalMode[] = {};
							gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_m120";
							gunnerOpticsEffect[] = {};
						};
					};

					class HitPoints {

						class HitGun {

							armor = 0.3;
							material = -1;
							name = "Turret";
							visual = "damage_visual";
							passThrough = 0;
							radius = 0.07;
						};
					};
				};
			};

			class assembleInfo
			{
				primary=0;
				base="";
				assembleTo="";
				displayName="";
				dissasembleTo[] = {

					"Redd_Tank_M120_Tampella_Barrel",
					"Redd_Tank_M120_Tampella_Tripod"
				};
			};

			class UserActions {

				class M120_removeflag {

					displayName = "$STR_Redd_flagge_entfernen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this getVariable 'has_flag')";
					statement = "[this,0] call Redd_fnc_m120_flags";
				};

				class M120_redFlag {

					displayName = "$STR_Redd_red_flagge_anbringen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,1] call Redd_fnc_m120_flags";
				};

				class M120_greenFlag {

					displayName = "$STR_Redd_green_flagge_anbringen";
					position = "actionPoint";
					radius = 2;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "!(this getVariable 'has_flag')";
					statement = "[this,2] call Redd_fnc_m120_flags";
				};
			};

			class EventHandlers {
				
				init = "_this call redd_fnc_m120_init";
				fired = "_this spawn redd_fnc_m120_fired";
			};

			class DestructionEffects {

				class Smoke {

					simulation = "particles";
					type = "WeaponWreckSmoke";
					position = "destructionEffect";
					intensity = 1;
					interval = 1;
					lifeTime = 5;
				};
			};
		};

		class Redd_Tank_M120_Tampella: Redd_Tank_M120_Tampella_Base {

			editorPreview="Redd_Tank_M120_Tampella\pictures\m_120_pre.paa";
			scope=2;
			scopeCurator = 2;
			displayname = "$STR_Redd_M120_Tampella";

			class textureSources {

				class Fleck {

					displayName = "$STR_Redd_M120_Tampella";
					author = "Redd,Tank";
					textures[]={"Redd_Tank_M120_Tampella\data\Redd_Tank_M120_Tampella_blend_co.paa"};
					factions[] = {"BLU_F"};
				};
			};

			textureList[]= {

				"Fleck", 1,

			};
		};
	};