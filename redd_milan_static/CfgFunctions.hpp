
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class milan_init
                {

                    file = "\redd_milan_static\functions\milan_init.sqf";

                };

                class milan_flags
                {

                    file = "\redd_milan_static\functions\milan_flags.sqf";

                };

            };

        };

    };