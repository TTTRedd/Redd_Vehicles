

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Wiesel_1A2_TOW_Driver = "Redd_Tank_Wiesel_1A2_TOW_Driver";
            Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out";

            Redd_Tank_Wiesel_1A2_TOW_Commander = "Redd_Tank_Wiesel_1A2_TOW_Commander";
            Redd_Tank_Wiesel_1A2_TOW_Commander_Out = "Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
            Redd_Tank_Wiesel_1A2_TOW_Commander_TOW = "Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";
            rnt_wieselTOW_com_out_high = "rnt_wieselTOW_com_out_high";
            rnt_wieselTOW_com_out_bino = "rnt_wieselTOW_com_out_bino";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";

            Redd_Tank_Wiesel_1A2_TOW_Loader = "Redd_Tank_Wiesel_1A2_TOW_Loader";
            Redd_Tank_Wiesel_1A2_TOW_Loader_Out = "Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
            Redd_Tank_Wiesel_1A2_TOW_Loader_MG = "Redd_Tank_Wiesel_1A2_TOW_Loader_MG";
            rnt_wieselTOW_loader_out_high = "rnt_wieselTOW_loader_out_high";
            rnt_wieselTOW_loader_out_bino = "rnt_wieselTOW_loader_out_bino";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG";

            //ffv poses
            rnt_wieselTOW_com_out_ffv = "rnt_wieselTOW_Unarmed_Idle_com";
            rnt_wieselTOW_loader_out_ffv = "rnt_wieselTOW_Unarmed_Idle_loader";

        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_wieselTOW_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_wieselTOW_Unarmed_Idle_com";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Idle_com";
                default = "rnt_wieselTOW_Unarmed_Idle_com";
                Stand = "rnt_wieselTOW_Unarmed_Idle_com";
                HandGunOn = "rnt_wieselTOW_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_wieselTOW_Unarmed_Idle_com";
                Binoculars = "rnt_wieselTOW_Binoc_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
                civil = "rnt_wieselTOW_Unarmed_Idle_com";
            };
            class rnt_wieselTOW_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_wieselTOW_Unarmed_Idle_com";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Idle_com";
                default = "rnt_wieselTOW_Unarmed_Idle_com";
                Stand = "rnt_wieselTOW_Unarmed_Idle_com";
                HandGunOn = "rnt_wieselTOW_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_wieselTOW_Unarmed_Idle_com";
                Binoculars = "rnt_wieselTOW_Unarmed_Binoc_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
                civil = "rnt_wieselTOW_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_wieselTOW_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_wieselTOW_Die_com";
                default = "rnt_wieselTOW_Die_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
            };
            class rnt_wieselTOW_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_wieselTOW_Die_com";
                default = "rnt_wieselTOW_Die_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
            };
            class rnt_wieselTOW_Pistol_Actions_com : rnt_wieselTOW_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselTOW_Pistol_com";
                stopRelaxed = "rnt_wieselTOW_Pistol_com";
                default = "rnt_wieselTOW_Pistol_com";
                Binoculars = "rnt_wieselTOW_Pistol_Binoc_com";
                Stand = "rnt_wieselTOW_Pistol_Idle_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_wieselTOW_Binoc_Actions_com : rnt_wieselTOW_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_wieselTOW_Pistol_Binoc_com";
                stopRelaxed = "rnt_wieselTOW_Pistol_Binoc_com";
                default = "rnt_wieselTOW_Binoc_com";
            };
            class rnt_wieselTOW_BinocPistol_Actions_com : rnt_wieselTOW_Binoc_Actions_com 
            {
                stop = "rnt_wieselTOW_Pistol_Binoc_com";
                stopRelaxed = "rnt_wieselTOW_Pistol_Binoc_com";
                default = "rnt_wieselTOW_Pistol_Binoc_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
            };
            class rnt_wieselTOW_BinocUnarmed_Actions_com : rnt_wieselTOW_Binoc_Actions_com 
            {
                stop = "rnt_wieselTOW_Unarmed_Binoc_com";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Binoc_com";
                default = "rnt_wieselTOW_Unarmed_Binoc_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
            };
            class rnt_wieselTOW_Idle_Actions_com : rnt_wieselTOW_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_wieselTOW_Idle_com";
                stopRelaxed = "rnt_wieselTOW_Idle_com";
                default = "rnt_wieselTOW_Idle_com";
                Combat = "rnt_wieselTOW_Unarmed_Idle_com";
                fireNotPossible = "rnt_wieselTOW_Unarmed_Idle_com";
                PlayerStand = "rnt_wieselTOW_Unarmed_Idle_com";
            };
            class rnt_wieselTOW_IdlePistol_Actions_com : rnt_wieselTOW_Action_Actions_com 
            {
                Stand = "rnt_wieselTOW_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselTOW_Pistol_Idle_com";
                stopRelaxed = "rnt_wieselTOW_Pistol_Idle_com";
                default = "rnt_wieselTOW_Pistol_Idle_com";
                Combat = "rnt_wieselTOW_Pistol_com";
                fireNotPossible = "rnt_wieselTOW_Pistol_com";
                PlayerStand = "rnt_wieselTOW_Pistol_com";
                die = "rnt_wieselTOW_Die_com";
                Unconscious = "rnt_wieselTOW_Die_com";
            };

            //Loader
            class rnt_wieselTOW_Action_Actions_loader : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_wieselTOW_Unarmed_Idle_loader";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Idle_loader";
                default = "rnt_wieselTOW_Unarmed_Idle_loader";
                Stand = "rnt_wieselTOW_Unarmed_Idle_loader";
                HandGunOn = "rnt_wieselTOW_Unarmed_Idle_loader";
                PrimaryWeapon = "rnt_wieselTOW_Unarmed_Idle_loader";
                Binoculars = "rnt_wieselTOW_Binoc_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
                civil = "rnt_wieselTOW_Unarmed_Idle_loader";
            };
            class rnt_wieselTOW_IdleUnarmed_Actions_loader : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_wieselTOW_Unarmed_Idle_loader";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Idle_loader";
                default = "rnt_wieselTOW_Unarmed_Idle_loader";
                Stand = "rnt_wieselTOW_Unarmed_Idle_loader";
                HandGunOn = "rnt_wieselTOW_Unarmed_Idle_loader";
                PrimaryWeapon = "rnt_wieselTOW_Unarmed_Idle_loader";
                Binoculars = "rnt_wieselTOW_Unarmed_Binoc_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
                civil = "rnt_wieselTOW_Unarmed_Idle_loader";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_wieselTOW_Dead_Actions_loader : FFV_BaseActions 
            {
                stop = "rnt_wieselTOW_Die_loader";
                default = "rnt_wieselTOW_Die_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
            };
            class rnt_wieselTOW_DeadPistol_Actions_loader : FFV_BaseActions 
            {
                stop = "rnt_wieselTOW_Die_loader";
                default = "rnt_wieselTOW_Die_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
            };
            class rnt_wieselTOW_Pistol_Actions_loader : rnt_wieselTOW_Action_Actions_loader 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselTOW_Pistol_loader";
                stopRelaxed = "rnt_wieselTOW_Pistol_loader";
                default = "rnt_wieselTOW_Pistol_loader";
                Binoculars = "rnt_wieselTOW_Pistol_Binoc_loader";
                Stand = "rnt_wieselTOW_Pistol_Idle_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_wieselTOW_Binoc_Actions_loader : rnt_wieselTOW_Action_Actions_loader 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_wieselTOW_Pistol_Binoc_loader";
                stopRelaxed = "rnt_wieselTOW_Pistol_Binoc_loader";
                default = "rnt_wieselTOW_Binoc_loader";
            };
            class rnt_wieselTOW_BinocPistol_Actions_loader : rnt_wieselTOW_Binoc_Actions_loader 
            {
                stop = "rnt_wieselTOW_Pistol_Binoc_loader";
                stopRelaxed = "rnt_wieselTOW_Pistol_Binoc_loader";
                default = "rnt_wieselTOW_Pistol_Binoc_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
            };
            class rnt_wieselTOW_BinocUnarmed_Actions_loader : rnt_wieselTOW_Binoc_Actions_loader 
            {
                stop = "rnt_wieselTOW_Unarmed_Binoc_loader";
                stopRelaxed = "rnt_wieselTOW_Unarmed_Binoc_loader";
                default = "rnt_wieselTOW_Unarmed_Binoc_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
            };
            class rnt_wieselTOW_Idle_Actions_loader : rnt_wieselTOW_Action_Actions_loader 
            {
                upDegree = "ManPosStand";
                stop = "rnt_wieselTOW_Idle_loader";
                stopRelaxed = "rnt_wieselTOW_Idle_loader";
                default = "rnt_wieselTOW_Idle_loader";
                Combat = "rnt_wieselTOW_Unarmed_Idle_loader";
                fireNotPossible = "rnt_wieselTOW_Unarmed_Idle_loader";
                PlayerStand = "rnt_wieselTOW_Unarmed_Idle_loader";
            };
            class rnt_wieselTOW_IdlePistol_Actions_loader : rnt_wieselTOW_Action_Actions_loader 
            {
                Stand = "rnt_wieselTOW_Pistol_Idle_loader";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselTOW_Pistol_Idle_loader";
                stopRelaxed = "rnt_wieselTOW_Pistol_Idle_loader";
                default = "rnt_wieselTOW_Pistol_Idle_loader";
                Combat = "rnt_wieselTOW_Pistol_loader";
                fireNotPossible = "rnt_wieselTOW_Pistol_loader";
                PlayerStand = "rnt_wieselTOW_Pistol_loader";
                die = "rnt_wieselTOW_Die_loader";
                Unconscious = "rnt_wieselTOW_Die_loader";
            };
        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;
            //Driver
            class Redd_Tank_Wiesel_1A2_TOW_Driver: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Tank_Wiesel_1A2_TOW_Driver_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Commander
            class Redd_Tank_Wiesel_1A2_TOW_Commander: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Tank_Wiesel_1A2_TOW_Commander_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class rnt_wieselTOW_com_out_high: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_high.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Tank_Wiesel_1A2_TOW_Commander_TOW: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander_TOW.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Loader
            class Redd_Tank_Wiesel_1A2_TOW_Loader: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Tank_Wiesel_1A2_TOW_Loader_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class rnt_wieselTOW_loader_out_high: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_high.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Tank_Wiesel_1A2_TOW_Loader_MG: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader_MG.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_wieselTOW_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_com";
                variantsAI[] = {"rnt_wieselTOW_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_wieselTOW_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_com", 0.1, "rnt_wieselTOW_Pistol_Idle_com", 0.2, "rnt_wieselTOW_Unarmed_Idle_com", 0.2, "rnt_wieselTOW_Die_com", 0.5};
             };
            class rnt_wieselTOW_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselTOW_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_com", 0.1, "rnt_wieselTOW_Pistol_Idle_com", 0.2, "rnt_wieselTOW_Unarmed_Idle_com", 0.2, "rnt_wieselTOW_Die_com", 0.5};
            };
            class rnt_wieselTOW_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_wieselTOW_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_com", 0.1, "rnt_wieselTOW_FromPistol_com", 0.1, "rnt_wieselTOW_Unarmed_Idle_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
                variantsAI[] = {"rnt_wieselTOW_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Pistol_Idle_Idling_com", 1};
            };
            class rnt_wieselTOW_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselTOW_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_com", 0.1, "rnt_wieselTOW_FromPistol_com", 0.1, "rnt_wieselTOW_Unarmed_Idle_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
            };
            class rnt_wieselTOW_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_high.rtm";
                actions = "rnt_wieselTOW_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_wieselTOW_FromBinoc_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
            };
            class rnt_wieselTOW_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_wieselTOW_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_FromBinoc_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
            };
            class rnt_wieselTOW_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_wieselTOW_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_End_com", 0.1, "rnt_wieselTOW_ToPistol_End_com", 0.1, "rnt_wieselTOW_Unarmed_ToBinoc_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
                variantsAI[] = {"rnt_wieselTOW_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_wieselTOW_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_End_com", 0.1, "rnt_wieselTOW_ToPistol_End_com", 0.1, "rnt_wieselTOW_Unarmed_ToBinoc_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
            };
            class rnt_wieselTOW_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_wieselTOW_Unarmed_FromBinoc_com", 0.1, "rnt_wieselTOW_Die_com", 0.1};
            };
            class rnt_wieselTOW_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_com_out_bino.rtm";
                actions = "rnt_wieselTOW_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Die_com : vehicle_turnout_1_Die 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                actions = "rnt_wieselTOW_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                actions = "rnt_wieselTOW_DeadPistol_Actions_com";
                showHandGun = 1;
            };

            //loader_out_ffv
            class rnt_wieselTOW_Pistol_loader : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_loader";
                variantsAI[] = {"rnt_wieselTOW_Pistol_Idling_loader", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Pistol_Idling_loader", 1};
                ConnectTo[] = {"rnt_wieselTOW_Pistol_ToBinoc_loader", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_loader", 0.1, "rnt_wieselTOW_Pistol_Idle_loader", 0.2, "rnt_wieselTOW_Unarmed_Idle_loader", 0.2, "rnt_wieselTOW_Die_loader", 0.5};
             };
            class rnt_wieselTOW_Pistol_Idling_loader : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselTOW_Pistol_loader", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_loader", 0.1, "rnt_wieselTOW_Pistol_Idle_loader", 0.2, "rnt_wieselTOW_Unarmed_Idle_loader", 0.2, "rnt_wieselTOW_Die_loader", 0.5};
            };
            class rnt_wieselTOW_Pistol_Idle_loader : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_wieselTOW_IdlePistol_Actions_loader";
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_loader", 0.1, "rnt_wieselTOW_FromPistol_loader", 0.1, "rnt_wieselTOW_Unarmed_Idle_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
                variantsAI[] = {"rnt_wieselTOW_Pistol_Idle_Idling_loader", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Pistol_Idle_Idling_loader", 1};
            };
            class rnt_wieselTOW_Pistol_Idle_Idling_loader : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselTOW_Pistol_Idle_loader", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_loader", 0.1, "rnt_wieselTOW_FromPistol_loader", 0.1, "rnt_wieselTOW_Unarmed_Idle_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
            };
            class rnt_wieselTOW_ToPistol_loader : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_ToPistol_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_ToPistol_End_loader : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_wieselTOW_Pistol_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromPistol_loader : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_high.rtm";
                actions = "rnt_wieselTOW_Pistol_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_FromPistol_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromPistol_End_loader : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Binoc_loader : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                InterpolateTo[] = {"rnt_wieselTOW_FromBinoc_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
            };
            class rnt_wieselTOW_Pistol_Binoc_loader : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_wieselTOW_BinocPistol_Actions_loader";
                InterpolateTo[] = {"rnt_wieselTOW_Pistol_FromBinoc_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
            };
            class rnt_wieselTOW_ToBinoc_loader : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_ToBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_ToBinoc_End_loader : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Binoc_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromBinoc_loader : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_FromBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_FromBinoc_End_loader : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_ToBinoc_loader : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_ToBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_ToBinoc_End_loader : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_Binoc_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_FromBinoc_loader : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_wieselTOW_Binoc_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_FromBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Pistol_FromBinoc_End_loader : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_wieselTOW_Action_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Pistol_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_Idle_loader : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_wieselTOW_IdleUnarmed_Actions_loader";
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_End_loader", 0.1, "rnt_wieselTOW_ToPistol_End_loader", 0.1, "rnt_wieselTOW_Unarmed_ToBinoc_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
                variantsAI[] = {"rnt_wieselTOW_Idle_Unarmed_Idling_loader", 1};
                variantsPlayer[] = {"rnt_wieselTOW_Idle_Unarmed_Idling_loader", 1};
            };
            class rnt_wieselTOW_Idle_Unarmed_Idling_loader : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_loader", 0.1};
                InterpolateTo[] = {"rnt_wieselTOW_FromPistol_End_loader", 0.1, "rnt_wieselTOW_ToPistol_End_loader", 0.1, "rnt_wieselTOW_Unarmed_ToBinoc_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
            };
            class rnt_wieselTOW_Unarmed_Binoc_loader : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_loader";
                InterpolateTo[] = {"rnt_wieselTOW_Unarmed_FromBinoc_loader", 0.1, "rnt_wieselTOW_Die_loader", 0.1};
            };
            class rnt_wieselTOW_Unarmed_ToBinoc_loader : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_ToBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_ToBinoc_End_loader : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Binoc_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_FromBinoc_loader : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_BinocUnarmed_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_FromBinoc_End_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Unarmed_FromBinoc_End_loader : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\rnt_wieselTOW_loader_out_bino.rtm";
                actions = "rnt_wieselTOW_Action_Actions_loader";
                ConnectTo[] = {"rnt_wieselTOW_Unarmed_Idle_loader", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Die_loader : vehicle_turnout_1_Die 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                actions = "rnt_wieselTOW_Dead_Actions_loader";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselTOW_Die_Pistol_loader : vehicle_turnout_1_Die_Pistol 
            {
                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                actions = "rnt_wieselTOW_DeadPistol_Actions_loader";
                showHandGun = 1;
            };
        };
    };