

	//Triggerd by BI eventhandler "getout"
	
	params["_veh","_pos","_unit","_turret"];

	//TOW
	if (_turret isEqualTo [0]) then
	{

		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];
		_veh animate ['Seat_L_Trans', 0];
		_veh animate ['TOW_Hide_Rohr', 1];

	};

	//MG3
	if (_turret isEqualTo [1]) then
	{

		[_veh,[[0,1],false]] remoteExecCall ['lockTurret'];
		_veh animate ['Seat_R_Trans', 0];

	};

	//check if unit is in bino turret
	if (_turret isEqualTo [2]) then
	{	
		
		_veh setVariable ['Redd_WieselTOW_Bino_In', false, true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];

	};

	//check if unit is in bino turret
	if (_turret isEqualTo [3]) then
	{	
		
		_veh setVariable ['Redd_WieselTOW_Bino2_In', false, true];
		[_veh,[[0,1],false]] remoteExecCall ['lockTurret'];

	};

	if ((_veh getVariable 'has_camonet_large') or (_veh getVariable 'has_camonet')) then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");

		_unit setUnitTrait ["camouflageCoef", _camouflage];

	};