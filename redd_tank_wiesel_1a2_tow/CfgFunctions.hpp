
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Wiesel_Init
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_Init.sqf";

                };

                class Wiesel_GetOut
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_getOut.sqf";

                };

                class Wiesel_GetIn
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_getIn.sqf";

                };
                
                class Wiesel_Bat_Komp
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_bat_komp.sqf";

                };

                class Wiesel_Plate
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_plate.sqf";

                };

                class wiesel_flags
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_flags.sqf";

                };

                class wiesel_camonet
                {

                    file = "\Redd_Tank_Wiesel_1A2_TOW\functions\Redd_Tank_Wiesel_1A2_TOW_camonet.sqf";

                };
                
            };

        };

    };