

	//marder init

	params["_veh"];

	[_veh] spawn redd_fnc_main_plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_marder_zug;//spawns function to set platoon letter and vehicle number
	[_veh] spawn redd_fnc_marder_bat_Komp;//spawns function to set battalion and company numbers
	
	_veh setVariable ['Redd_Marder_Bino_In', false,true];//initiates varibale for "climp up" and "climp down" function of turned out commander
	_veh setVariable ['Redd_Marder_Commander_Winkelspiegel', false,true];//initiates varibale for "periskope mirror" function of commander
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet

	[_veh,[[1],true]] remoteExecCall ["lockTurret"]; //locks Milan
	[_veh,[[2],true]] remoteExecCall ['lockTurret']; //Locks Bino turret