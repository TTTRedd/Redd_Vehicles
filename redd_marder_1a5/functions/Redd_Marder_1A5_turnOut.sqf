

	//triggered by BI eventhandler "turnOut"
	
	params ["_veh","_unit","_turret"];

	//checks if commander turns out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Marder_Commander_Up', false,true]; //resets variable for "climp up" function 
		_veh setVariable ['Redd_Marder_Commander_Winkelspiegel', false,true];//resets variable for "periscope mirrors" function 

	};