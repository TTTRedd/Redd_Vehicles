

	//Function for Eden attributes to set battalion and company number

	if (!isServer) exitWith {};
	
	params["_veh"];

	_b = _veh getVariable ["Redd_Marder_1A5_Bataillon",""];
	_k = _veh getVariable ["Redd_Marder_1A5_Kompanie",""];
	
	//sets battalion
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_33_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_33_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_92_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_92_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_112_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_112_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_122_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_122_ca.paa"];
		
		};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_212_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_212_ca.paa"];
		
		};
		
		case ("6"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_371_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_371_ca.paa"];
		
		};
		
		case ("7"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_391_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_391_ca.paa"];
		
		};
		
		case ("8"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_401_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_401_ca.paa"];
		
		};
		
		case ("9"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Bataillon_411_ca.paa"];
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Bataillon_411_ca.paa"];
		
		};

		case ("10"):
		{
		
			_r = selectRandom [33,92,112,122,212,371,391,401,411];
			_veh setObjectTextureGlobal [16, format["\Redd_Vehicles_Main\data\Redd_Bataillon_%1_ca.paa",_r]];
			_veh setObjectTextureGlobal [18, format["\Redd_Vehicles_Main\data\Redd_Bataillon_%1_ca.paa",_r]];
		
		};

	};
	
	//sets company
	switch toLower (_k) do 
	{
	
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Kompanie_1_ca.paa"];
			_veh setObjectTextureGlobal [19, "\Redd_Vehicles_Main\data\Redd_Kompanie_1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Kompanie_2_ca.paa"];
			_veh setObjectTextureGlobal [19, "\Redd_Vehicles_Main\data\Redd_Kompanie_2_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Kompanie_3_ca.paa"];
			_veh setObjectTextureGlobal [19, "\Redd_Vehicles_Main\data\Redd_Kompanie_3_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Kompanie_4_ca.paa"];
			_veh setObjectTextureGlobal [19, "\Redd_Vehicles_Main\data\Redd_Kompanie_4_ca.paa"];
		
		};

		case ("5"):
		{
		
			_r = selectRandom [1,2,3,4];
			_veh setObjectTextureGlobal [17, format["\Redd_Vehicles_Main\data\Redd_Kompanie_%1_ca.paa",_r]];
			_veh setObjectTextureGlobal [19, format["\Redd_Vehicles_Main\data\Redd_Kompanie_%1_ca.paa",_r]];

		};
		
	};