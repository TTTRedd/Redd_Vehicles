

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Marder_Driver = "Redd_Marder_Driver";
            Redd_Marder_Driver_Out = "Redd_Marder_Driver_Out";
            KIA_Redd_Marder_Driver = "KIA_Redd_Marder_Driver";
            KIA_Redd_Marder_Driver_Out = "KIA_Redd_Marder_Driver_Out";

            rnt_marder_com = "rnt_marder_com";
            Redd_Marder_Commander_Winkelspiegel = "Redd_Marder_Commander_Winkelspiegel";
            Redd_Marder_Commander_Milan = "Redd_Marder_Commander_Milan";
            rnt_marder_com_out = "rnt_marder_com_out";
            rnt_marder_com_out_high = "rnt_marder_com_out_high";
            rnt_marder_com_out_bino = "rnt_marder_com_out_bino";
            KIA_rnt_marder_com = "KIA_rnt_marder_com";
            KIA_Redd_Marder_Commander_Winkelspiegel = "KIA_Redd_Marder_Commander_Winkelspiegel";
            KIA_Redd_Marder_Commander_Milan = "KIA_Redd_Marder_Commander_Milan";
            KIA_rnt_marder_com_out = "KIA_rnt_marder_com_out";

            Redd_Marder_Gunner = "Redd_Marder_Gunner";
            KIA_Redd_Marder_Gunner = "KIA_Redd_Marder_Gunner";

            Redd_Marder_Passenger_Back = "Redd_Marder_Passenger_Back";
            Redd_Marder_Passenger = "Redd_Marder_Passenger";
            KIA_Redd_Marder_Passenger_Back = "KIA_Redd_Marder_Passenger_Back";
            KIA_Redd_Marder_Passenger = "KIA_Redd_Marder_Passenger";

            //ffv poses
            rnt_marder_com_out_ffv = "rnt_marder_Unarmed_Idle_com"; 

        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_marder_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_marder_Unarmed_Idle_com";
                stopRelaxed = "rnt_marder_Unarmed_Idle_com";
                default = "rnt_marder_Unarmed_Idle_com";
                Stand = "rnt_marder_Unarmed_Idle_com";
                HandGunOn = "rnt_marder_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_marder_Unarmed_Idle_com";
                Binoculars = "rnt_marder_Binoc_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
                civil = "rnt_marder_Unarmed_Idle_com";
            };
            class rnt_marder_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_marder_Unarmed_Idle_com";
                stopRelaxed = "rnt_marder_Unarmed_Idle_com";
                default = "rnt_marder_Unarmed_Idle_com";
                Stand = "rnt_marder_Unarmed_Idle_com";
                HandGunOn = "rnt_marder_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_marder_Unarmed_Idle_com";
                Binoculars = "rnt_marder_Unarmed_Binoc_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
                civil = "rnt_marder_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_marder_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_marder_Die_com";
                default = "rnt_marder_Die_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
            };
            class rnt_marder_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_marder_Die_com";
                default = "rnt_marder_Die_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
            };
            class rnt_marder_Pistol_Actions_com : rnt_marder_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_marder_Pistol_com";
                stopRelaxed = "rnt_marder_Pistol_com";
                default = "rnt_marder_Pistol_com";
                Binoculars = "rnt_marder_Pistol_Binoc_com";
                Stand = "rnt_marder_Pistol_Idle_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_marder_Binoc_Actions_com : rnt_marder_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_marder_Pistol_Binoc_com";
                stopRelaxed = "rnt_marder_Pistol_Binoc_com";
                default = "rnt_marder_Binoc_com";
            };
            class rnt_marder_BinocPistol_Actions_com : rnt_marder_Binoc_Actions_com 
            {
                stop = "rnt_marder_Pistol_Binoc_com";
                stopRelaxed = "rnt_marder_Pistol_Binoc_com";
                default = "rnt_marder_Pistol_Binoc_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
            };
            class rnt_marder_BinocUnarmed_Actions_com : rnt_marder_Binoc_Actions_com 
            {
                stop = "rnt_marder_Unarmed_Binoc_com";
                stopRelaxed = "rnt_marder_Unarmed_Binoc_com";
                default = "rnt_marder_Unarmed_Binoc_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
            };
            class rnt_marder_Idle_Actions_com : rnt_marder_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_marder_Idle_com";
                stopRelaxed = "rnt_marder_Idle_com";
                default = "rnt_marder_Idle_com";
                Combat = "rnt_marder_Unarmed_Idle_com";
                fireNotPossible = "rnt_marder_Unarmed_Idle_com";
                PlayerStand = "rnt_marder_Unarmed_Idle_com";
            };
            class rnt_marder_IdlePistol_Actions_com : rnt_marder_Action_Actions_com 
            {
                Stand = "rnt_marder_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_marder_Pistol_Idle_com";
                stopRelaxed = "rnt_marder_Pistol_Idle_com";
                default = "rnt_marder_Pistol_Idle_com";
                Combat = "rnt_marder_Pistol_com";
                fireNotPossible = "rnt_marder_Pistol_com";
                PlayerStand = "rnt_marder_Pistol_com";
                die = "rnt_marder_Die_com";
                Unconscious = "rnt_marder_Die_com";
            };
        };
    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Marder_Driver: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Driver",1};
                ConnectTo[]={"KIA_Redd_Marder_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Marder_Driver_Out: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Marder_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class KIA_Redd_Marder_Driver: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Marder_Driver_Out: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            
            class rnt_marder_com: Crew
            {

                file = "\Redd_Marder_1A5\anims\rnt_marder_com.rtm";
                interpolateTo[] = {"KIA_rnt_marder_com",1};
                ConnectTo[]={"KIA_rnt_marder_com", 1};
                 
            };
            class rnt_marder_com_out: Crew
            {

                file = "\Redd_Marder_1A5\anims\rnt_marder_com_out.rtm";
                interpolateTo[] = {"KIA_rnt_marder_com_out",1};
                ConnectTo[]={"KIA_rnt_marder_com_out", 1};

            };
            class rnt_marder_com_out_high: Crew
            {

                file = "\Redd_Marder_1A5\anims\rnt_marder_com_out_high.rtm";
                interpolateTo[] = {"KIA_rnt_marder_com_out",1};
                ConnectTo[]={"KIA_rnt_marder_com_out", 1};

            };
            class Redd_Marder_Commander_Winkelspiegel: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Winkelspiegel.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Winkelspiegel",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Winkelspiegel", 1};

            };
            class Redd_Marder_Commander_Milan: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Milan.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Milan",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Milan", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1};

            };
            class KIA_rnt_marder_com: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_rnt_marder_com.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_rnt_marder_com_out: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_rnt_marder_com_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Marder_Commander_Winkelspiegel: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Winkelspiegel.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Marder_Commander_Milan: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Milan.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Gunner: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Gunner.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Gunner",1};
                ConnectTo[]={"KIA_Redd_Marder_Gunner", 1};

            };
            class KIA_Redd_Marder_Gunner: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Gunner.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Passenger_Back: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Passenger_Back.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Passenger_Back",1};
                ConnectTo[]={"KIA_Redd_Marder_Passenger_Back", 1};
                canPullTrigger = 0;
                disableWeapons=1;
			    disableWeaponsLong=1;

            };
            class KIA_Redd_Marder_Passenger_Back: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Passenger_Back.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Passenger: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Passenger.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Passenger",1};
                ConnectTo[]={"KIA_Redd_Marder_Passenger", 1};
                canPullTrigger = 0;
                disableWeapons=1;
			    disableWeaponsLong=1;

            };
            class KIA_Redd_Marder_Passenger: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Passenger.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_marder_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_marder_Pistol_Actions_com";
                variantsAI[] = {"rnt_marder_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_marder_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_marder_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_marder_FromPistol_com", 0.1, "rnt_marder_Pistol_Idle_com", 0.2, "rnt_marder_Unarmed_Idle_com", 0.2, "rnt_marder_Die_com", 0.5};
             };
            class rnt_marder_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_marder_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_marder_FromPistol_com", 0.1, "rnt_marder_Pistol_Idle_com", 0.2, "rnt_marder_Unarmed_Idle_com", 0.2, "rnt_marder_Die_com", 0.5};
            };
            class rnt_marder_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_marder_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_marder_Pistol_com", 0.1, "rnt_marder_FromPistol_com", 0.1, "rnt_marder_Unarmed_Idle_com", 0.1, "rnt_marder_Die_com", 0.1};
                variantsAI[] = {"rnt_marder_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_marder_Pistol_Idle_Idling_com", 1};
            };
            class rnt_marder_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_marder_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_marder_Pistol_com", 0.1, "rnt_marder_FromPistol_com", 0.1, "rnt_marder_Unarmed_Idle_com", 0.1, "rnt_marder_Die_com", 0.1};
            };
            class rnt_marder_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_marder_Pistol_Actions_com";
                ConnectTo[] = {"rnt_marder_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_marder_Pistol_Actions_com";
                ConnectTo[] = {"rnt_marder_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_high.rtm";
                actions = "rnt_marder_Pistol_Actions_com";
                ConnectTo[] = {"rnt_marder_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_marder_Action_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_marder_FromBinoc_com", 0.1, "rnt_marder_Die_com", 0.1};
            };
            class rnt_marder_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_marder_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_marder_Pistol_FromBinoc_com", 0.1, "rnt_marder_Die_com", 0.1};
            };
            class rnt_marder_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_marder_Action_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_marder_Binoc_Actions_com";
                ConnectTo[] = {"rnt_marder_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_marder_Action_Actions_com";
                ConnectTo[] = {"rnt_marder_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_marder_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_marder_FromPistol_End_com", 0.1, "rnt_marder_ToPistol_End_com", 0.1, "rnt_marder_Unarmed_ToBinoc_com", 0.1, "rnt_marder_Die_com", 0.1};
                variantsAI[] = {"rnt_marder_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_marder_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_marder_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_marder_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_marder_FromPistol_End_com", 0.1, "rnt_marder_ToPistol_End_com", 0.1, "rnt_marder_Unarmed_ToBinoc_com", 0.1, "rnt_marder_Die_com", 0.1};
            };
            class rnt_marder_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_marder_Unarmed_FromBinoc_com", 0.1, "rnt_marder_Die_com", 0.1};
            };
            class rnt_marder_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\redd_marder_1a5\anims\rnt_marder_com_out_bino.rtm";
                actions = "rnt_marder_Action_Actions_com";
                ConnectTo[] = {"rnt_marder_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Die_com : vehicle_turnout_1_Die 
            {
                file = "\redd_marder_1a5\anims\KIA_rnt_marder_com_out.rtm";
                actions = "rnt_marder_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_marder_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\redd_marder_1a5\anims\KIA_rnt_marder_com_out.rtm";
                actions = "rnt_marder_DeadPistol_Actions_com";
                showHandGun = 1;
            };

        };

    };