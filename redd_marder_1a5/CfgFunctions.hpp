
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Marder_Init
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_init.sqf";

                };

                class Marder_Fired
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_fired.sqf";

                };

                class Marder_GetOut
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_getOut.sqf";

                };

                class luchs_GetIn
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_getIn.sqf";

                };

                class Marder_TurnIn
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_turnIn.sqf";

                };

                class Marder_TurnOut
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_turnOut.sqf";

                };

                class Marder_Zug
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_zug.sqf";

                };

                class Marder_Bat_Komp
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_bat_komp.sqf";

                };

                class marder_flags
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_flags.sqf";

                };

                class marder_camonet
                {

                    file = "\Redd_Marder_1A5\functions\Redd_Marder_1A5_camonet.sqf";

                };

            };

        };

    };