	
	
	class RenderTargets /// class for all Picture-in-Picture (PiP)
	{
		
		class LeftMirror
		{
			
			renderTarget = "rendertarget0"; /// what texture is going to be used, it should be mapped in model like #(argb,256,512,1)r2t(rendertarget0,1.0)
			
			class CameraView1
			{
				
				pointPosition		= "PIP0_pos"; 	/// memory point of PiP origin
				pointDirection		= "PIP0_dir"; 	/// memory point of PiP direction
				renderQuality 		= 2;			/// what quality should the PiP be
				renderVisionMode 	= 0;			/// zero means standard vision
				fov 				= 0.7;			/// what is the angle of the PiP field of view
			}; 
			
		};
		
		class RightMirror	/// rendering of PiPs is shared, it is best to have one for all sources, this is just an example
		{
			
			renderTarget = "rendertarget1";
			
			class CameraView1
			{
				
				pointPosition		= "PIP1_pos";
				pointDirection		= "PIP1_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
				
			}; 
			
		};
		
		class KomTagOptic	/// rendering of PiPs is shared, it is best to have one for all sources, this is just an example
		{
			
			renderTarget = "rendertarget2";
			
			class CameraView1
			{
				
				pointPosition		= "gunnerview";
				pointDirection		= "gunnerviewDir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
				
			}; 
			
		};
		
		class KomTermOptic	/// rendering of PiPs is shared, it is best to have one for all sources, this is just an example
		{
			
			renderTarget = "rendertarget3";
			
			class CameraView1
			{
				
				pointPosition		= "gunnerview";
				pointDirection		= "gunnerviewDir";
				renderQuality 		= 2;
				renderVisionMode 	= 2;
				fov 				= 0.7;	
				
			}; 
			
		};
		
		class Rueckfahrkamera	/// rendering of PiPs is shared, it is best to have one for all sources, this is just an example
		{
			
			renderTarget = "rendertarget4";
			
			class CameraView1
			{
				
				pointPosition		= "rueckfahrview";
				pointDirection		= "rueckfahrviewDir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
				
			}; 
			
		};

	};