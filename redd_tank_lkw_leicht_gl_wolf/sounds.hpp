			
			soundGetIn[] = {"A3\Sounds_F\vehicles\soft\Truck_02\getin", 0.562341, 1};
			soundGetOut[] = {"A3\Sounds_F\vehicles\soft\Truck_02\getout", 0.562341, 1, 20};
			soundDammage[] = {"", 0.562341, 1};

			/*
			soundEngineOnInt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_start", 0.707946, 1};
			soundEngineOnExt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_start", 0.707946, 1, 200};
			soundEngineOffInt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_stop", 0.707946, 1};
			soundEngineOffExt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_stop", 0.707946, 1, 200};
			*/
			soundEngineOnInt[] = {"A3\Sounds_F_Exp\vehicles\soft\Offroad_02\engine_ext_start", 0.7, 1};
			soundEngineOnExt[] = {"A3\Sounds_F_Exp\vehicles\soft\Offroad_02\engine_ext_start", 0.7, 1, 200};
			soundEngineOffInt[] = {"A3\Sounds_F_Exp\vehicles\soft\Offroad_02\engine_ext_stop", 0.7, 1};
			soundEngineOffExt[] = {"A3\Sounds_F_Exp\vehicles\soft\Offroad_02\engine_ext_stop", 0.7, 1, 200};


			buildCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1, 1, 200};
			buildCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1, 1, 200};
			buildCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1, 1, 200};
			buildCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1, 1, 200};
			soundBuildingCrash[] = {"buildCrash0", 0.25, "buildCrash1", 0.25, "buildCrash2", 0.25, "buildCrash3", 0.25};
			WoodCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1, 1, 200};
			WoodCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1, 1, 200};
			WoodCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1, 1, 200};
			WoodCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1, 1, 200};
			soundWoodCrash[] = {"woodCrash0", 0.25, "woodCrash1", 0.25, "woodCrash2", 0.25, "woodCrash3", 0.25};
			armorCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1, 1, 200};
			armorCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1, 1, 200};
			armorCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1, 1, 200};
			armorCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1, 1, 200};
			soundArmorCrash[] = {"ArmorCrash0", 0.25, "ArmorCrash1", 0.25, "ArmorCrash2", 0.25, "ArmorCrash3", 0.25};
			Crash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1, 1, 200};
			Crash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1, 1, 200};
			Crash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1, 1, 200};
			Crash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1, 1, 200};
			soundCrashes[] = {"Crash0", 0.25, "Crash1", 0.25, "Crash2", 0.25, "Crash3", 0.25};

			class Sounds
			{
				//Ext
				class Engine
				{

					sound[]	=	{"A3\Sounds_F\vehicles\soft\Hatchback_01\Hatchback_01_ext_2000rpm",	0.65,1,200};
					frequency = "0.6  + (rpm factor[0, 6000])";
        			volume = "engineOn * camPos * (0.5 + (rpm factor[0, 6000]) * 3.5)";

				};
				class EngineThrust
				{

					sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_01", 1, 1, 200};
					frequency = "0.6  + (rpm factor[0, 6000])";
    				volume = "engineOn * camPos *     (thrust factor[0.1,1]) * (rpm factor[0, 6000])";

				};

				//Int
				class Engine_int
				{
				
					sound[]	=	{"A3\Sounds_F\vehicles\soft\Hatchback_01\Hatchback_01_int_2000rpm",	0.35,1};
					frequency = "0.6  + (rpm factor[0, 6000])";
        			volume = "engineOn * (1-camPos) * (0.5 + (rpm factor[0, 6000]) * 3.5)";

				};
				class EngineThrust_int
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_00", 0.55, 1};
					frequency = "0.6  + (rpm factor[0, 6000])";
    				volume = "engineOn * (1-camPos) * (thrust factor[0.1,1]) * (rpm factor[0, 6000])";

				};
				
				class Movement
				{
					sound = "soundEnviron";
					frequency = "1";
					volume = "0";
				};
				class TiresRockOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_1", 1, 1, 60};
					frequency = "1";
					volume = "camPos*rock*(speed factor[2, 20])";
				};
				class TiresSandOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-sand1", 1, 1, 60};
					frequency = "1";
					volume = "camPos*sand*(speed factor[2, 20])";
				};
				class TiresGrassOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_2", 1, 1, 60};
					frequency = "1";
					volume = "camPos*grass*(speed factor[2, 20])";
				};
				class TiresMudOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-mud2", 1, 1, 60};
					frequency = "1";
					volume = "camPos*mud*(speed factor[2, 20])";
				};
				class TiresGravelOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_gravel_1", 1, 1, 60};
					frequency = "1";
					volume = "camPos*gravel*(speed factor[2, 20])";
				};
				class TiresAsphaltOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_asfalt_2", 1, 1, 60};
					frequency = "1";
					volume = "camPos*asphalt*(speed factor[2, 20])";
				};
				class NoiseOut
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", 1, 1, 90};
					frequency = "1";
					volume = "camPos*(damper0 max 0.02)*(speed factor[0, 8])";
				};
				class TiresRockIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_1", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*rock*(speed factor[2, 20])";
				};
				class TiresSandIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-sand2", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*sand*(speed factor[2, 20])";
				};
				class TiresGrassIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_2", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*grass*(speed factor[2, 20])";
				};
				class TiresMudIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-mud2", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*mud*(speed factor[2, 20])";
				};
				class TiresGravelIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_gravel_1", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*gravel*(speed factor[2, 20])";
				};
				class TiresAsphaltIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_asfalt_2", 0.707946, 1};
					frequency = "1";
					volume = "(1-camPos)*asphalt*(speed factor[2, 20])";
				};
				class NoiseIn
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", 0.707946, 1};
					frequency = "1";
					volume = "(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
				};
				class breaking_ext_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04", 0.707946, 1, 80};
					frequency = 1;
					volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 10])";
				};
				class acceleration_ext_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02", 0.707946, 1, 80};
					frequency = 1;
					volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
				};
				class turn_left_ext_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02", 0.707946, 1, 80};
					frequency = 1;
					volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
				};
				class turn_right_ext_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02", 0.707946, 1, 80};
					frequency = 1;
					volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
				};
				class breaking_ext_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking", 0.707946, 1, 60};
					frequency = 1;
					volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 10])";
				};
				class acceleration_ext_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\acceleration_dirt_ext_1", 0.707946, 1, 60};
					frequency = 1;
					volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
				};
				class turn_left_ext_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt", 0.707946, 1, 60};
					frequency = 1;
					volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
				};
				class turn_right_ext_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt", 0.707946, 1, 60};
					frequency = 1;
					volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
				};
				class breaking_int_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 6])";
				};
				class acceleration_int_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
				};
				class turn_left_int_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
				};
				class turn_right_int_road
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
				};
				class breaking_int_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 6])";
				};
				class acceleration_int_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\acceleration_dirt_int_1", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
				};
				class turn_left_int_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
				};
				class turn_right_int_dirt
				{
					sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int", 0.630957, 1};
					frequency = 1;
					volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
				};
			};