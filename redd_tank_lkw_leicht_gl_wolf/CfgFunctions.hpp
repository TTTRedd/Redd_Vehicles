
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class wolf_init
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_init.sqf";

                };

                class wolf_w_init
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_w_init.sqf";

                };

                class wolf_flags
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_flags.sqf";

                };

                class wolf_camonet
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_camonet.sqf";

                };

                class wolf_plate
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_plate.sqf";

                };

                class wolf_getIn
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_getIn.sqf";

                };

                class wolf_getOut
                {

                    file = "\Redd_Tank_LKW_leicht_gl_Wolf\functions\Redd_Tank_LKW_leicht_gl_Wolf_getOut.sqf";

                };

            };

        };

    };