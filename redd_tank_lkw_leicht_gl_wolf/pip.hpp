	
	
	class RenderTargets
	{
		
		class LeftMirror
		{
			
			renderTarget = "rendertarget0";
			
			class CameraView1
			{
				
				pointPosition		= "PIP0_pos";
				pointDirection		= "PIP0_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;
			}; 
			
		};
		
		class RightMirror
		{
			
			renderTarget = "rendertarget1";
			
			class CameraView1
			{
				
				pointPosition		= "PIP1_pos";
				pointDirection		= "PIP1_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
				
			}; 
			
		};
		
	};