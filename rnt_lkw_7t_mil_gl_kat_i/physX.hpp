	
	
	//Basic parameters
	simulation = carx;
	dampersBumpCoef = 0.05;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 300 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 300;
	
	//Differential parameters
	differentialType = "all_limited";
	frontRearSplit = 0.5;
	frontBias = 1.3;
	rearBias = 1.3;
	centreBias = 1.3;

	//Engine parameters
	maxOmega = 366.52;
	minOmega = 52.36;
	enginePower = 373;
	peakTorque = 1600;
	idleRPM = 500;
	redRPM = 3500;
	clutchStrength = 32;
	dampingRateFullThrottle = 0.08;
	dampingRateZeroThrottleClutchEngaged = 2;
	dampingRateZeroThrottleClutchDisengaged = 0.35;	
	maxSpeed = 90;
	thrustDelay = 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef=0.535;
	slowSpeedForwardCoef=0.2;

	torqueCurve[] = {
		{"(0/3500)","(0/1600)"},
		{"(500/3500)","(1600/1600)"},
		{"(1000/3500)","(1600/1600)"},
		{"(1500/3500)","(1600/1600)"},
		{"(2000/3500)","(1600/1600)"},
		{"(2600/3500)","(1350/1600)"},
		{"(3000/3500)","(1100/1600)"},
		{"(3500/3500)","(850/1600)"}
	};

	//Floating and sinking
	waterPPInVehicle=0;
	maxFordingDepth = -1.2;
	waterResistance = 0;
	canFloat = 0;
	waterLeakiness = 10;

	//Anti-roll bars
	antiRollbarForceCoef = 200;
	antiRollbarForceLimit = 200;
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 90;
	
	//Gearbox
	class complexGearbox
	{
		GearboxRatios[]={
			
			"R1",-7,
			"N",0,
			"D1",5,
			"D2",4,
			"D3",3.2,
			"D4",2.6,
			"D5",2.2,
			"D6",1.925
		};

		TransmissionRatios[]={"High",5};

		gearBoxMode="auto";
		moveOffGear=1;
		driveString="D";
		neutralString="N";
		reverseString="R";
	};
	
	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{
		1.0,0.45,
		0.6,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		1.0,0.6
	};
	
	switchTime = 0.5;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference = 4.08;
	numberPhysicalWheels = 6;
	turnCoef = 2.5;

	class Wheels 
	{
		class LF 
		{
			side="left";
			suspTravelDirection[]={-0.125,-1,0};
			boneName="wheel_1_1_damper";
			steering=1;
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			width="0.35";
			mass=80;
			MOI=17;
			dampingRate=0.1;
			dampingRateDamaged=1;
			dampingRateDestroyed=1000;
			maxBrakeTorque=10000;
			maxHandBrakeTorque=0;
			suspForceAppPointOffset="wheel_1_1_axis";
			tireForceAppPointOffset="wheel_1_1_axis";
			maxCompression=0.15;
			maxDroop=0.15;
			sprungMass=3333;
			springStrength=140819;
			springDamperRate=28164;
			longitudinalStiffnessPerUnitGravity=10000;
			latStiffX=25;
			latStiffY=180;
			frictionVsSlipGraph[]=
			{
				{0,1},
				{0.5,1},
				{1,1}
			};

		};
		class LR: LF
		{
			boneName="wheel_1_2_damper";
			steering=0;
			center="wheel_1_2_axis";
			boundary="wheel_1_2_bound";
			suspForceAppPointOffset="wheel_1_2_axis";
			tireForceAppPointOffset="wheel_1_2_axis";
			maxHandBrakeTorque=10000;
			
		};
		class LR2: LF
		{
			boneName="wheel_1_3_damper";
			steering=0;
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			suspForceAppPointOffset="wheel_1_3_axis";
			tireForceAppPointOffset="wheel_1_3_axis";
			maxHandBrakeTorque=10000;
			
		}
		class RF: LF
		{
			side="right";
			suspTravelDirection[]={0.125,-1,0};
			boneName="wheel_2_1_damper";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			suspForceAppPointOffset="wheel_2_1_axis";
			tireForceAppPointOffset="wheel_2_1_axis";
			
		};
		class RR: RF
		{
			boneName="wheel_2_2_damper";
			steering=0;
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			suspForceAppPointOffset="wheel_2_2_axis";
			tireForceAppPointOffset="wheel_2_2_axis";
			maxHandBrakeTorque=10000;

		};

		class RR2: RF
		{
			boneName="wheel_2_3_damper";
			steering=0;
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			suspForceAppPointOffset="wheel_2_3_axis";
			tireForceAppPointOffset="wheel_2_3_axis";
			maxHandBrakeTorque=10000;

		};
		
	};