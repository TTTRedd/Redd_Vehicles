

class CfgPatches {
    class rnt_lkw_7t_mil_gl_kat_i {
        units[] = {
            "rnt_lkw_7t_mil_gl_kat_i_transport_fleck",
            "rnt_lkw_7t_mil_gl_kat_i_transport_trope",
            "rnt_lkw_7t_mil_gl_kat_i_transport_winter",
            
            "rnt_lkw_7t_mil_gl_kat_i_mun_fleck",
            "rnt_lkw_7t_mil_gl_kat_i_mun_trope",
            "rnt_lkw_7t_mil_gl_kat_i_mun_winter"
        };
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"A3_Soft_F","A3_Soft_F_Beta","Redd_Vehicles_Main","rnt_t_base"};
    };
};