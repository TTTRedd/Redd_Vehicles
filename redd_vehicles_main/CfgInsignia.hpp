

	class CfgUnitInsignia
	{
		
		class Redd_JFST
		{

			displayName = "Redd 00 JFST";
			texture = "\Redd_Vehicles_Main\icon\00_JFST_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_Gef
		{

			displayName = "Redd 01 Gefreiter";
			texture = "\Redd_Vehicles_Main\icon\01_Gefr_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OGef
		{

			displayName = "Redd 02 Obergefreiter";
			texture = "\Redd_Vehicles_Main\icon\02_OGefr_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_HptGef
		{

			displayName = "Redd 03 Hauptgefreiter";
			texture = "\Redd_Vehicles_Main\icon\03_HptGefr_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_StGefr
		{

			displayName = "Redd 04 Stabsgefreiter";
			texture = "\Redd_Vehicles_Main\icon\04_StGefr_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OStGefr
		{

			displayName = "Redd 05 Oberstabsgefreiter";
			texture = "\Redd_Vehicles_Main\icon\05_OStGefr_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_Uffz
		{

			displayName = "Redd 06 Unteroffizier";
			texture = "\Redd_Vehicles_Main\icon\06_Uffz_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_StUffz
		{

			displayName = "Redd 07 Stabsunteroffizier";
			texture = "\Redd_Vehicles_Main\icon\07_StUffz_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_FW
		{

			displayName = "Redd 08 Feldwebel";
			texture = "\Redd_Vehicles_Main\icon\08_FW_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OFw
		{

			displayName = "Redd 09 Oberfeldwebel";
			texture = "\Redd_Vehicles_Main\icon\09_OFw_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_HptFw
		{

			displayName = "Redd 10 Hauptfeldwebel";
			texture = "\Redd_Vehicles_Main\icon\10_HptFw_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_StFw
		{

			displayName = "Redd 11 Stabsfeldwebel";
			texture = "\Redd_Vehicles_Main\icon\11_StFw_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OStFw
		{

			displayName = "Redd 12 Oberstabsfeldwebel";
			texture = "\Redd_Vehicles_Main\icon\12_OStFw_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_Lt
		{

			displayName = "Redd 13 Leutnant";
			texture = "\Redd_Vehicles_Main\icon\13_Lt_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OLt
		{

			displayName = "Redd 14 Oberleutnant";
			texture = "\Redd_Vehicles_Main\icon\14_OLt_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_Hptm
		{

			displayName = "Redd 15 Hauptmann";
			texture = "\Redd_Vehicles_Main\icon\15_Hptm_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_StHptm
		{

			displayName = "Redd 16 Stabshauptmann";
			texture = "\Redd_Vehicles_Main\icon\16_StHptm_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_Maj
		{

			displayName = "Redd 17 Major";
			texture = "\Redd_Vehicles_Main\icon\17_Maj_ca.paa";
			textureVehicle = "";
			
		};

		class Redd_OberstLt
		{

			displayName = "Redd 18 Oberstleutnant";
			texture = "\Redd_Vehicles_Main\icon\18_OberstLt_ca.paa";
			textureVehicle = "";
			
		};
		
	};