	
	
	class CfgPatches
	{
		
		class Redd_Vehicles_Main
		{
			
			units[] = 
			{

				"Redd_Milan_Rohr",
				"Redd_Wiesel_TOW_Wreck",
				"Redd_Wiesel_MK20_Wreck",
				"Redd_Fuchs_Wreck",
				"Redd_Marder_Wreck",
				"Redd_Gepard_Wreck",
				"Redd_Wolf_Wreck",
				"Redd_Box_120mm_Mo_HE",
				"Redd_Box_120mm_Mo_HE_annz",
				"Redd_Box_120mm_Mo_Smoke",
				"Redd_Box_120mm_Mo_Illum",
				"Redd_Box_120mm_Mo_Combo"

			};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Weapons_F","A3_Sounds_f"};
			
		};
		
	};