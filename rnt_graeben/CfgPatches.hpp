

class CfgPatches{
	class rnt_graeben{
        units[] = {
			"rnt_graben_bunker",
			"rnt_graben_ecke",
			"rnt_graben_ende",
			"rnt_graben_gerade",
			"rnt_graben_stand",
			"rnt_graben_stellung",
			"rnt_graben_t"
		};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
    };
};