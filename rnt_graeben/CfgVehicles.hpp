

    class CfgVehicles {

        class BagBunker_base_F;

        class rnt_graben_bunker: BagBunker_base_F {
            author = "rnt";
            mapSize = 5;
            editorPreview = "rnt_graeben\pictures\bunker.paa";
			scope = 2;
		    scopeCurator = 2;
            displayName = "$STR_rnt_graben_bunker";
            model = "\rnt_graeben\rnt_graben_bunker";
            icon = "iconObject_1x1";
			editorCategory = "EdCat_Structures";
            editorSubcategory = "Redd_Structures";
            
            class SimpleObject {
				eden = 0;
				animate[] = {};
				hide[] = {};
				verticalOffset = 0.966;
				verticalOffsetWorld = 0;
				init = "''";
			};
		}; 

        class rnt_graben_ecke: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\ecke.paa";
            displayName = "$STR_rnt_graben_ecke";
            model = "\rnt_graeben\rnt_graben_ecke";
        };

        class rnt_graben_ende: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\ende.paa";
            displayName = "$STR_rnt_graben_ende";
            model = "\rnt_graeben\rnt_graben_ende";
        };

        class rnt_graben_gerade: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\gerade.paa";
            displayName = "$STR_rnt_graben_gerade";
            model = "\rnt_graeben\rnt_graben_gerade";
        };

        class rnt_graben_stand: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\stand.paa";
            displayName = "$STR_rnt_graben_stand";
            model = "\rnt_graeben\rnt_graben_stand";
        };

        class rnt_graben_stellung: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\stellung.paa";
            displayName = "$STR_rnt_graben_stellung";
            model = "\rnt_graeben\rnt_graben_stellung";
        };

        class rnt_graben_t: rnt_graben_bunker {
            editorPreview = "rnt_graeben\pictures\T.paa";
            displayName = "$STR_rnt_graben_t";
            model = "\rnt_graeben\rnt_graben_t";
        };
    };