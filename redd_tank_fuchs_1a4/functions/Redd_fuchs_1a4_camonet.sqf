

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Sets camonet
//			 
//	Example: ;
//				 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  1: STRING - Camonet, Camonet_large
//				  2: OBJECT - Player
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_vehicle","_type","_unit"];

_fuelEventhandler = 0; 

if (_type isEqualTo "camonet") then {

	if !(_vehicle getVariable 'has_camonet') then {

		_vehicle setVariable ['has_camonet', true,true];

		switch ((typeOf _vehicle)) do {

			case "Redd_Tank_Fuchs_1A4_Jg_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_San_Flecktarn": {
				_vehicle animateSource ["fuchsWanneFleck_hide_source",0];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_San_Tropentarn": {
				_vehicle animateSource ["fuchsWanneTrope_hide_source",0];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_San_Wintertarn": {
				_vehicle animateSource ["fuchsWanneWinter_hide_source",0];
			};
		};

		{
			_unit_class = typeOf _x;
			_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
			_camouflage_new = _camouflage/100*80;
			_x setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

		} forEach crew _vehicle;

	} else {

		_vehicle setVariable ['has_camonet', false,true];

		switch ((typeOf _vehicle)) do {
			
			case "Redd_Tank_Fuchs_1A4_Jg_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_San_Flecktarn": {
				_vehicle animateSource ["fuchsWanneFleck_hide_source",1];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_San_Tropentarn": {
				_vehicle animateSource ["fuchsWanneTrope_hide_source",1];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_San_Wintertarn": {
				_vehicle animateSource ["fuchsWanneWinter_hide_source",1];
			};

		};
		
		{
			_unit_class = typeOf _x;
			_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
			_x setUnitTrait ["camouflageCoef", _camouflage];

		} forEach crew _vehicle;
	};
};

if (_type isEqualTo "camonet_large") then {

	if !(_vehicle getVariable 'has_camonet_large') then {

		_vehicle setVariable ['has_camonet_large', true,true];

		_vehicle engineOn false;
		private _handler = _vehicle addEventHandler ["Engine", {
			params ["_vehicle", "_engineState"];
			if (_engineState) then {
				_vehicle engineOn false;
			};
		}];
		_vehicle setVariable ['ReddEngineEH', _handler,true];

		switch ((typeOf _vehicle)) do {

			case "Redd_Tank_Fuchs_1A4_Jg_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_San_Flecktarn": {
				_vehicle animateSource ["fuchsBodenFleck_hide_source",0];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_San_Tropentarn": {
				_vehicle animateSource ["fuchsBodenTrope_hide_source",0];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_San_Wintertarn": {
				_vehicle animateSource ["fuchsBodenWinter_hide_source",0];
			};
		};

		{_x setUnitTrait ["camouflageCoef", 0.1];} forEach crew _vehicle;

	} else {

		_vehicle setVariable ['has_camonet_large', false,true];

		_handler = _vehicle getVariable ['ReddEngineEH', -1];
		if (_handler > -1) then {
			_vehicle removeEventHandler ["Engine", _handler];
		};

		switch (typeOf _vehicle) do
		{
			case "Redd_Tank_Fuchs_1A4_Jg_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Flecktarn";
			case "Redd_Tank_Fuchs_1A4_San_Flecktarn":{
				_vehicle animateSource ["fuchsBodenFleck_hide_source",1];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Tropentarn";
			case "Redd_Tank_Fuchs_1A4_San_Tropentarn": {
				_vehicle animateSource ["fuchsBodenTrope_hide_source",1];
			};

			case "Redd_Tank_Fuchs_1A4_Jg_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_Pi_Wintertarn";
			case "Redd_Tank_Fuchs_1A4_San_Wintertarn": {
				_vehicle animateSource ["fuchsBodenWinter_hide_source",1];
			};
		};
		
		{
			_unit_class = typeOf _x;
			_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
			_x setUnitTrait ["camouflageCoef", _camouflage];

		} forEach crew _vehicle;
	};
};

true