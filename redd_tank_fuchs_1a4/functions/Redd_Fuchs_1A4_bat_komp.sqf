

	//Function for Eden attributes to set battalion and company number

	if (!isServer) exitWith {};

	params ["_veh"];

	_w = _veh getVariable ["Redd_Tank_Fuchs_Jg_1A4_Waffengattung",""];
	_w1 = _veh getVariable ["Redd_Tank_Fuchs_Pi_1A4_Waffengattung",""];
	_b = _veh getVariable ["Redd_Tank_Fuchs_Jg_1A4_Bataillon",""];
	_b2 = _veh getVariable ["Redd_Tank_Fuchs_Pi_1A4_Bataillon",""];
	_k = _veh getVariable ["Redd_Tank_Fuchs_Jg_1A4_Kompanie",""];

	//sets unit
	switch toLower (_w) do
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Jg.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\GebJg.paa"];
		
		};

		case ("3"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\PzAufkl.paa"];
		
		};
		
	};

	switch toLower (_w1) do
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Pi.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\PzPi.paa"];
		
		};

		case ("3"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\GebPi.paa"];
		
		};

	};
	
	//sets battalion
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_91_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_291_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_292_ca.paa"];
		
		};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_413_ca.paa"];
		
		};

		case ("6"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_231_ca.paa"];
		
		};

		case ("7"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_232_ca.paa"];
		
		};

		case ("8"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_233_ca.paa"];
		
		};

		case ("9"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_5_ca.paa"];
		
		};

		case ("10"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_6_ca.paa"];
		
		};

		case ("11"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_7_ca.paa"];
		
		};

		case ("12"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_8_ca.paa"];
		
		};

		case ("13"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_12_ca.paa"];
		
		};

		case ("14"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_13_ca.paa"];
		
		};

	};

	switch toLower (_b2) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_905_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_901_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_130_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_1_ca.paa"];
		
		};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_803_ca.paa"];
		
		};

		case ("6"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_4_ca.paa"];
		
		};

		case ("7"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_701_ca.paa"];
		
		};

		case ("8"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_8_ca.paa"];
		
		};

	};
	
	//sets company
	switch toLower (_k) do 
	{
	
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Kompanie_1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Kompanie_2_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Kompanie_3_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Kompanie_4_ca.paa"];
		
		};

		case ("5"):
		{
		
			_veh setObjectTextureGlobal [18, "\Redd_Vehicles_Main\data\Redd_Kompanie_5_ca.paa"];
		
		};

	};