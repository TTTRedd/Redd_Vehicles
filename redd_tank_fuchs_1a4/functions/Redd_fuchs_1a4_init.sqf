

	params ["_veh"];

	[_veh] spawn redd_fnc_fuchs_plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_fuchs_bat_Komp;//spawns function to set battalion and company numbers

	_veh setVariable ['Redd_Fuchs_MG3_In', false, true];
	_veh setVariable ['Redd_Fuchs_Milan_In', false, true];
	_veh setVariable ['Redd_Fuchs_Bino_In', false, true];
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet

	[_veh,[[0],true]] remoteExecCall ['lockTurret']; //Locks MG3 turret
	[_veh,[[1],true]] remoteExecCall ['lockTurret']; //Locks Bino
	[_veh,[[2],true]] remoteExecCall ['lockTurret']; //Locks Milan