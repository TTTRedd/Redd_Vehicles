

    class CfgPatches
	{
		
		class Redd_Bags
		{
			
			units[] = 
			{

				"Redd_Milan_Static_Barrel",
				"Redd_Milan_Static_Tripod",

				"Redd_Tank_M120_Tampella_Barrel",
				"Redd_Tank_M120_Tampella_Tripod",

                "rnt_mg3_static_barell",
				"rnt_mg3_static_tripod",

                "rnt_gmw_static_barell",
				"rnt_gmw_static_tripod"

			};

			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"Redd_Vehicles_Main"};
			
		};
		
	};

    class CfgVehicles
    {

        class Bag_Base;

        class Weapon_Bag_Base: Bag_Base
        {

            class assembleInfo{};

        };

        class Redd_Milan_Static_Barrel: Weapon_Bag_Base
		{

			faction = "BLU_F";
			author="Redd";
			editorCategory = "EdCat_Equipment";
			editorSubcategory = "EdSubcat_DismantledWeapons";
			hiddenSelectionsTextures[]={};
			mass=380;
            scope = 2;
			displayName = "$STR_Redd_Milan_Tube";
			model = "\Redd_Backpacks\Redd_Milan_Static_Barrel.p3d";
			editorPreview="\Redd_Backpacks\pictures\Milan_Bag_Pre_Picture.paa";
			picture = "\Redd_Backpacks\pictures\redd_milan_barrel_ui_pre_ca.paa";

			class assembleInfo: assembleInfo
			{

				displayName = "$STR_Redd_Milan";
				assembleTo="Redd_Milan_Static";
				base[] = {"Redd_Milan_Static_Tripod"};

			};
			
		};

		class Redd_Milan_Static_Tripod: Bag_Base 
		{

			author="Redd";
			faction = "BLU_F";
			editorCategory="EdCat_Equipment";
			editorSubcategory="EdSubcat_DismantledWeapons";
			hiddenSelectionsTextures[]={};
			icon="iconBackpack";
			mass=180;
			maximumLoad=0;
            scope = 2;
			displayName = "$STR_Redd_Milan_Tripod";
			model = "\Redd_Backpacks\Redd_Milan_Static_Tripod.p3d";
			editorPreview="\Redd_Backpacks\pictures\Milan_Tripod_Pre_Picture.paa";
			picture = "\Redd_Backpacks\pictures\redd_milan_tripod_ui_pre_ca.paa";

			class assembleInfo
			{

				primary=0;
				base="";
				assembleTo="";
				dissasembleTo[]={};
				displayName="";

			};

		};
            
        class Redd_Tank_M120_Tampella_Barrel: Weapon_Bag_Base
        {

            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=380;
            scope = 2;
            displayName = "$STR_Redd_m120_barrel";
            model = "\Redd_Backpacks\Redd_Tank_M120_Tampella_Barrel.p3d";
            editorPreview="\Redd_Backpacks\pictures\m_120_barrel_pre.paa";
            picture = "\Redd_Backpacks\pictures\redd_m120_barrel_ui_pre_ca.paa";

            class assembleInfo: assembleInfo 
            {

                displayName = "M120 Tampella";
                assembleTo="Redd_Tank_M120_Tampella";
                base[] = {"Redd_Tank_M120_Tampella_Tripod"};

            };

        };

        class Redd_Tank_M120_Tampella_Tripod: Bag_Base 
        {

            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_Redd_m120_base";
            model = "\Redd_Backpacks\Redd_Tank_M120_Tampella_Tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\m_120_Tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\redd_m120_tripod_ui_pre_ca.paa";

            class assembleInfo
            {
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };

        };

        class rnt_mg3_static_barell: Weapon_Bag_Base {
            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=250;
            scope = 2;
            displayName = "$STR_rnt_mg3_static_barell";
            model = "\Redd_Backpacks\rnt_mg3_static_barell.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_mg3_static_barell_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_mg3_static_barell_ui_pre_ca.paa";

            class assembleInfo: assembleInfo {
                displayName = "$STR_rnt_mg3_static";
                assembleTo="rnt_mg3_static";
                base[] = {"rnt_mg3_static_tripod"};
            };
        };

        class rnt_mg3_static_tripod: Bag_Base {
            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_rnt_mg3_static_tripod";
            model = "\Redd_Backpacks\rnt_mg3_static_tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_mg3_static_tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_mg3_static_tripod_ui_pre_ca.paa";

            class assembleInfo{
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };
        };

        class rnt_mg3_static_barell_ai: Weapon_Bag_Base {
            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=250;
            scope = 2;
            displayName = "$STR_rnt_mg3_static_barell_ai";
            model = "\Redd_Backpacks\rnt_mg3_static_barell.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_mg3_static_barell_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_mg3_static_barell_ui_pre_ca.paa";

            class assembleInfo: assembleInfo {
                displayName = "$STR_rnt_mg3_static_ai";
                assembleTo="rnt_mg3_static_ai";
                base[] = {"rnt_mg3_static_tripod_ai"};
            };
        };

        class rnt_mg3_static_tripod_ai: Bag_Base {
            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_rnt_mg3_static_tripod_ai";
            model = "\Redd_Backpacks\rnt_mg3_static_tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_mg3_static_tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_mg3_static_tripod_ui_pre_ca.paa";

            class assembleInfo{
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };
        };

        class rnt_gmw_static_barell: Weapon_Bag_Base {
            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=250;
            scope = 2;
            displayName = "$STR_rnt_gmw_static_barell";
            model = "\Redd_Backpacks\rnt_gmw_static_barell.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_gmw_static_barell_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_gmw_static_barell_ui_pre_ca.paa";

            class assembleInfo: assembleInfo {
                displayName = "$STR_rnt_gmw_static";
                assembleTo="rnt_gmw_static";
                base[] = {"rnt_gmw_static_tripod"};
            };
        };

        class rnt_gmw_static_tripod: Bag_Base {
            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_rnt_gmw_static_tripod";
            model = "\Redd_Backpacks\rnt_gmw_static_tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_gmw_static_tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_gmw_static_tripod_ui_pre_ca.paa";

            class assembleInfo{
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };
        };

        class rnt_gmw_static_barell_ai: Weapon_Bag_Base {
            faction = "BLU_F";
            author="Redd";
            editorCategory = "EdCat_Equipment";
            editorSubcategory = "EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            mass=250;
            scope = 2;
            displayName = "$STR_rnt_gmw_static_barell_ai";
            model = "\Redd_Backpacks\rnt_gmw_static_barell.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_gmw_static_barell_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_gmw_static_barell_ui_pre_ca.paa";

            class assembleInfo: assembleInfo {
                displayName = "$STR_rnt_gmw_static_ai";
                assembleTo="rnt_gmw_static_ai";
                base[] = {"rnt_gmw_static_tripod_ai"};
            };
        };

        class rnt_gmw_static_tripod_ai: Bag_Base {
            author="Redd";
            faction = "BLU_F";
            editorCategory="EdCat_Equipment";
            editorSubcategory="EdSubcat_DismantledWeapons";
            hiddenSelectionsTextures[]={};
            icon="iconBackpack";
            mass=180;
            maximumLoad=0;
            scope = 2;
            displayName = "$STR_rnt_gmw_static_tripod_ai";
            model = "\Redd_Backpacks\rnt_gmw_static_tripod.p3d";
            editorPreview="\Redd_Backpacks\pictures\rnt_gmw_static_tripod_pre.paa";
            picture = "\Redd_Backpacks\pictures\rnt_gmw_static_tripod_ui_pre_ca.paa";

            class assembleInfo{
                primary=0;
                base="";
                assembleTo="";
                dissasembleTo[]={};
                displayName="";
            };
        };
    };
    