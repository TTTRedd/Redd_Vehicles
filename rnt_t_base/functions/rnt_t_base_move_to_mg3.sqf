

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Animates mg3 hatch and moves co-driver to mg3
//			 
//	Example: n/a
//				 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  1: OBJECT - Unit
//				  
//	Returns: true
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_vehicle","_unit"];

if !(_vehicle getVariable 'rnt_t_base_MG3_In') then {
	_vehicle setVariable ['rnt_t_base_MG3_In', true, true];
	_vehicle animateSource ["mg_hatch_source",1];
	waitUntil {_vehicle animationSourcePhase "mg_hatch_source" == 1};
	_unit action ['moveToTurret', _vehicle, [0]];
	[_vehicle,[[1],true]] remoteExecCall ['lockTurret'];
} else {
	_unit action ['moveToTurret', _vehicle, [1]];
	[_vehicle,[[1],false]] remoteExecCall ['lockTurret'];
	_vehicle animateSource ["mg_hatch_source",0];
	waitUntil {_vehicle animationSourcePhase "mg_hatch_source" == 0};
	_vehicle setVariable ['rnt_t_base_MG3_In', false, true];
};

true