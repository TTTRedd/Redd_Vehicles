

	//Triggerd by BI eventhandler "getout"
	
	params ["_veh","_pos","_unit","_turret"];
	
	//check if unit is in mg3
	if (_turret isEqualTo [0]) then {	
		_veh setVariable ['rnt_t_base_MG3_In', false, true];
		[_veh,[[1],false]] remoteExecCall ['lockTurret'];
	};