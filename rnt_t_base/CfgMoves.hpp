

class CfgMovesBasic {

    class DefaultDie;
    
    class ManActions {
        rnt_tonner_Driver = "rnt_tonner_Driver";
        rnt_tonner_CoDriver = "rnt_tonner_CoDriver";
        rnt_tonner_CoDriver_mg = "rnt_tonner_CoDriver_mg";
       
        KIA_rnt_tonner_Driver = "KIA_rnt_tonner_Driver";
        KIA_rnt_tonner_CoDriver = "KIA_rnt_tonner_CoDriver";
        KIA_rnt_tonner_CoDriver_mg = "KIA_rnt_tonner_CoDriver_mg";
    };
};

class CfgMovesMaleSdr: CfgMovesBasic {

    class States {

        class Crew;

        //Driver
        class rnt_tonner_Driver: Crew {
            file = "\rnt_t_base\anims\rnt_tonner_Driver.rtm";
            interpolateTo[] = {"KIA_rnt_tonner_Driver",1};
            ConnectTo[]={"KIA_rnt_tonner_Driver", 1};
            leftHandIKCurve[] = {1}; 
            rightHandIKCurve[] = {1}; 
        };
        class KIA_rnt_tonner_Driver: DefaultDie{
            file = "\rnt_t_base\anims\KIA_rnt_tonner_Driver.rtm";
            actions = "DeadActions";
            terminal = 1;
            speed = 0.5;
            ragdoll = 1;
            connectTo[] = {"Unconscious",0.1};
            InterpolateTo[] = {};
        }; 

        //CoDriver
        class rnt_tonner_CoDriver: Crew {
            file = "\rnt_t_base\anims\rnt_tonner_CoDriver.rtm";
            interpolateTo[] = {"KIA_rnt_tonner_CoDriver",1};
            ConnectTo[]={"KIA_rnt_tonner_CoDriver", 1};
            leftHandIKCurve[] = {1}; 
            rightHandIKCurve[] = {1}; 
        };   
        class KIA_rnt_tonner_CoDriver: DefaultDie {
            file = "\rnt_t_base\anims\KIA_rnt_tonner_CoDriver.rtm";
            actions = "DeadActions";
            terminal = 1;
            speed = 0.5;
            ragdoll = 1;
            connectTo[] = {"Unconscious",0.1};
            InterpolateTo[] = {};
        }; 

        //CoDriverMG
        class rnt_tonner_CoDriver_mg: Crew {
            file = "\rnt_t_base\anims\rnt_tonner_CoDriver_mg.rtm";
            interpolateTo[] = {"KIA_rnt_tonner_CoDriver_mg",1};
            ConnectTo[]={"KIA_rnt_tonner_CoDriver_mg", 1};
            leftHandIKCurve[] = {1}; 
            rightHandIKCurve[] = {1}; 
        }; 
        class KIA_rnt_tonner_CoDriver_mg: DefaultDie {
            file = "\rnt_t_base\anims\KIA_rnt_tonner_CoDriver_mg.rtm";
            actions = "DeadActions";
            terminal = 1;
            speed = 0.5;
            ragdoll = 1;
            connectTo[] = {"Unconscious",0.1};
            InterpolateTo[] = {};
        };     
    };
};