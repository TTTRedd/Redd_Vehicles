

class CfgPatches {
    class rnt_lkw_5t_mil_gl_kat_i {
        units[] = {
            "rnt_lkw_5t_mil_gl_kat_i_transport_fleck",
            "rnt_lkw_5t_mil_gl_kat_i_transport_trope",
            "rnt_lkw_5t_mil_gl_kat_i_transport_winter",
            
            "rnt_lkw_5t_mil_gl_kat_i_fuel_fleck",
            "rnt_lkw_5t_mil_gl_kat_i_fuel_trope",
            "rnt_lkw_5t_mil_gl_kat_i_fuel_winter"
        };
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"A3_Soft_F","A3_Soft_F_Beta","Redd_Vehicles_Main","rnt_t_base"};
    };
};