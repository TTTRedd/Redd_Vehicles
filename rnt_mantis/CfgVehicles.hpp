class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class DefaultVehicleSystemsDisplayManagerLeft{
	class components;
};
class DefaultVehicleSystemsDisplayManagerRight{
	class components;
};
class VehicleSystemsTemplateLeftPilot: DefaultVehicleSystemsDisplayManagerLeft{
	class components;
};
class VehicleSystemsTemplateRightPilot: DefaultVehicleSystemsDisplayManagerRight{
	class components;
};

class Eventhandlers;

class CfgVehicles 
{
    class NonStrategic;
    class StaticShip;
    class Building;
    class House_F;
    class FloatingStructure_F;
    class thingx;
    class Ship;
    class LandVehicle;
    class StaticWeapon: LandVehicle {
        class Turrets;
        class HitPoints;
    };
    class StaticMGWeapon: StaticWeapon {
        class Turrets: Turrets {
            class MainTurret;
        };
        class Components;
    };
    class rnt_mantis_base: StaticMGWeapon {
        model = "\rnt_mantis\rnt_mantis";
        editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Static";
        displayname = "Mantis GDF-020";
        scope=2;
        scopeCurator=2;
        side=1;
        crew="B_UAV_AI";
        typicalCargo[]={
            "B_UAV_AI"
        };
        author="RnT";
        picture="\A3\Static_F_Jets\AAA_system_01\Data\UI\AAA_system_01_picture_CA.paa";
		uiPicture="\A3\Static_F_Jets\AAA_system_01\Data\UI\AAA_system_01_picture_CA.paa";
		icon="\A3\Static_F_Jets\AAA_system_01\Data\UI\AAA_system_01_icon_CA.paa";
        hasDriver=0;
        hasGunner=1;
        isUav=1;
        uavCameraGunnerPos="pos_gunner_view";
        uavCameraGunnerDir="pos_gunner_view_dir";
        memoryPointGun="pos_barrel_end";
        threat[]={0.30000001,0.30000001,1};
        cost=150000;
        accuracy=0.12;
        editorPreview="rnt_mantis\pictures\mantis_gesch.paa";
        unitInfoType="RscUnitInfoTank";
    
        hiddenSelections[]= {
            "kanone_base",
            "kanone_turm"
        };
        hiddenSelectionsTextures[]= {
            "rnt_mantis\data\rnt_mantis_geschuetz_kanone_base_blend_co",
            "rnt_mantis\data\rnt_mantis_geschuetz_kanone_turm_blend_co"
        };

        extCameraPosition[]={0,1.5,-10};
        canFloat=0;
        enableGPS=1;
        radartype=2;
        radarTarget=1;
        radarTargetSize=0.89999998;
        visualTarget=1;
        visualTargetSize=1.2;
        irTarget=1;
        irTargetSize=0.5;
        reportRemoteTargets=1;
        receiveRemoteTargets=1;
        reportOwnPosition=1;
        lockDetectionSystem=0;
        incomingMissileDetectionSystem=16;
        animated=1;
        armor=80;
        armorStructural=2;
        damageResistance=0.004;
        damageEffect="AirDestructionEffects";
        
        class Components: Components {
            class SensorsManagerComponent {
                class Components {
                    class IRSensorComponent: SensorTemplateIR {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=4000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=3500;
                            objectDistanceLimitCoef=1;
                            viewDistanceLimitCoef=1;
                        };
                        typeRecognitionDistance=3500;
                        maxTrackableSpeed=600;
                        angleRangeHorizontal=60;
                        angleRangeVertical=40;
                        animDirection="mainGun";
                        aimDown=-0.5;
                    };
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar {
                        class AirTarget {
                            minRange=10000;
                            maxRange=10000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget {
                            minRange=7000;
                            maxRange=7000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        typeRecognitionDistance=7000;
                        angleRangeHorizontal=360;
                        angleRangeVertical=100;
                        aimDown=-45;
                        maxTrackableSpeed=1388.89;
                    };
                    class DataLinkSensorComponent: SensorTemplateDataLink{};
                };
            };
        };
        
        class HitPoints: HitPoints {
            class HitHull {
                armor=3;
                name="hit_hull";
                visual="damage_visual";
                radius=0.25;
                minimalHit=0.050000001;
                explosionShielding=0.2;
                depends="Total";
                passThrough=0.1;
                material=51;
            };
            class HitTurret: HitHull {
                armor=3;
                name="hit_turret";
                convexComponent="turret";
                visual="damage_visual";
                passThrough=0.1;
                minimalHit=0.1;
                explosionShielding=0.2;
                
                class DestructionEffects {
                    class light1 {
                        simulation="light";
                        type="ObjectDestructionLight";
                        position="turretdestruct_pos";
                        intensity=0.001;
                        interval=1;
                        lifeTime=3;
                    };
                    class smoke1 {
                        simulation="particles";
                        type="ObjectDestructionSmoke";
                        position="turretdestruct_pos";
                        intensity=0.15000001;
                        interval=1;
                        lifeTime=3.5;
                    };
                    class fire1 {
                        simulation="particles";
                        type="ObjectDestructionFire1";
                        position="turretdestruct_pos";
                        intensity=0.15000001;
                        interval=1;
                        lifeTime=3;
                    };
                    class sparks1 {
                        simulation="particles";
                        type="ObjectDestructionSparks";
                        position="turretdestruct_pos";
                        intensity=0;
                        interval=1;
                        lifeTime=0;
                    };
                    class sound {
                        simulation="sound";
                        position="turretdestruct_pos";
                        intensity=1;
                        interval=1;
                        lifeTime=1;
                        type="Fire";
                    };
                };
                
            };
            class Hitgun: HitTurret {
                name="hit_gun";
                visual="damage_visual";
                convexComponent="gun";
                armor=2;
                passThrough=0.1;
                explosionShielding=1;
                radius=0.12;
                
                class DestructionEffects: DestructionEffects
                {
                    class light1: light1
                    {
                        position="gundestruct_pos";
                    };
                    class smoke1: smoke1
                    {
                        position="gundestruct_pos";
                    };
                    class fire1: fire1
                    {
                        position="gundestruct_pos";
                    };
                    class sparks1: sparks1
                    {
                        position="gundestruct_pos";
                    };
                    class sound: sound
                    {
                        position="gundestruct_pos";
                    };
                };
            };
        };
        
        class Damage {
            tex[]={};
            mat[]= {
                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_base.rvmat",
                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_base_damage.rvmat",
                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_base_destruct.rvmat",

                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_turm.rvmat",
                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_turm_damage.rvmat",
                "rnt_mantis\mats\rnt_mantis_geschuetz_kanone_turm_destruct.rvmat"
            };
        };
        
        class Turrets: Turrets {
            class MainTurret: MainTurret {
                minelev=-15;
                maxelev=85;
                minturn=-180;
                maxturn=180;
                initElev=15;
                initTurn=0;
                maxHorizontalRotSpeed=2.7;
                maxVerticalRotSpeed=2.7;
                soundServo[]= {
                    "A3\Sounds_F\vehicles\armor\noises\servo_best",
                    1.4125376,
                    1,
                    40
                };
                hasGunner=1;
                gunnerName="STR_mantis_gunner";
                primary=1;
                primaryGunner=1;
                startEngine=0;
                enableManualFire=1;
                turretinfotype="RscOptics_APC_Tracked_01_gunner";
                optics=1;
                gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
                class OpticsIn {
                    class Wide {
                        opticsDisplayName="W";
                        initAngleX=0;
                        minAngleX=-30;
                        maxAngleX=30;
                        initAngleY=0;
                        minAngleY=-100;
                        maxAngleY=100;
                        initFov=0.46599999;
                        minFov=0.46599999;
                        maxFov=0.46599999;
                        visionMode[]= {
                            "Normal",
                            "NVG",
                            "Ti"
                        };
                        thermalMode[]={0,1};
                        gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
                    };
                    class Medium: Wide {
                        opticsDisplayName="M";
                        initFov=0.093000002;
                        minFov=0.093000002;
                        maxFov=0.093000002;
                        gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_m_F";
                    };
                    class Narrow: Wide {
                        opticsDisplayName="N";
                        gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_n_F";
                        initFov=0.028999999;
                        minFov=0.028999999;
                        maxFov=0.028999999;
                    };
                };
                forceHideGunner=1;
                gunnerforceoptics=1;
                gunnerOutForceOptics=1;
                viewgunnerinExternal=0;
                outGunnerMayFire=1;
                inGunnerMayFire=1;
                castGunnerShadow=0;
                showAllTargets=2;
                body="MainTurret";
                gun="MainGun";
                animationSourceBody="MainTurret";
                animationSourceGun="MainGun";
                gunbeg="pos_barrel_end";
                gunend="pos_barrel";
                uavCameraGunnerPos="pos_gunner_view";
                uavCameraGunnerDir="pos_gunner_view_dir";
                memoryPointGunnerOptics="pos_gunner_view";
                particlespos="pos_fx";
                particlesdir="pos_fx_dir";
                selectionFireAnim="zasleh";
                gunnerlefthandanimname="";
                gunnerrighthandanimname="";
                weapons[]=
                {
                    "Redd_35mm_mantis"
                };
                magazines[]=
                {
                    "Redd_35mm_HE_2_Mag",
                    "Redd_35mm_HE_2_Mag"
                };
                class Components: Components {
                    class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft {
                        class Components {
                            class EmptyDisplay {
                                componentType="EmptyDisplayComponent";
                            };
                            class MinimapDisplay {
                                componentType="MinimapDisplayComponent";
                                resource="RscCustomInfoMiniMap";
                            };
                            class UAVDisplay {
                                componentType="UAVFeedDisplayComponent";
                            };
                            class SensorDisplay {
                                componentType="SensorsDisplayComponent";
                                range[]={16000,8000,4000,2000};
                                resource="RscCustomInfoSensors";
                            };
                        };
                    };
                    class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight {
                        defaultDisplay="SensorDisplay";
                        class Components
                        {
                            class EmptyDisplay {
                                componentType="EmptyDisplayComponent";
                            };
                            class MinimapDisplay {
                                componentType="MinimapDisplayComponent";
                                resource="RscCustomInfoMiniMap";
                            };
                            class UAVDisplay {
                                componentType="UAVFeedDisplayComponent";
                            };
                            class SensorDisplay {
                                componentType="SensorsDisplayComponent";
                                range[]={16000,8000,4000,2000};
                                resource="RscCustomInfoSensors";
                            };
                        };
                    };
                };
            };
        };

        class AttributeValues{
            RadarUsageAI=1;
        };

        class AnimationSources {

            class recoil_source {
                source = "reload"; 
                weapon = "Redd_35mm_mantis";
            };

            class 35mm_muzzle_source {
                source = "reload"; 
                weapon = "Redd_35mm_mantis";	
			};
        };
    };
};