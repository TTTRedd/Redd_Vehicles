

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Gepard_Driver = "Redd_Gepard_Driver";
            Redd_Gepard_Driver_Out = "Redd_Gepard_Driver_Out";
            KIA_Redd_Gepard_Driver = "KIA_Redd_Gepard_Driver";
            KIA_Redd_Gepard_Driver_Out = "KIA_Redd_Gepard_Driver_Out";

            Redd_Gepard_Commander = "Redd_Gepard_Commander";
            Redd_Gepard_Commander_Out = "Redd_Gepard_Commander_Out";
            Redd_Gepard_Commander_Out_high = "Redd_Gepard_Commander_Out_high";
            Redd_Gepard_Commander_Out_bino = "Redd_Gepard_Commander_Out_bino";
            KIA_Redd_Gepard_Commander = "KIA_Redd_Gepard_Commander";
            KIA_Redd_Gepard_Commander_Out = "KIA_Redd_Gepard_Commander_Out";

            Redd_Gepard_Gunner = "Redd_Gepard_Gunner";
            Redd_Gepard_Gunner_Out = "Redd_Gepard_Gunner_Out";
            KIA_Redd_Gepard_Gunner = "KIA_Redd_Gepard_Gunner";
            KIA_Redd_Gepard_Gunner_Out = "KIA_Redd_Gepard_Gunner_Out";

            //ffv poses
            Redd_Gepard_Commander_out_ffv = "rnt_gepard_Unarmed_Idle_com"; 

        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_gepard_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_gepard_Unarmed_Idle_com";
                stopRelaxed = "rnt_gepard_Unarmed_Idle_com";
                default = "rnt_gepard_Unarmed_Idle_com";
                Stand = "rnt_gepard_Unarmed_Idle_com";
                HandGunOn = "rnt_gepard_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_gepard_Unarmed_Idle_com";
                Binoculars = "rnt_gepard_Binoc_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
                civil = "rnt_gepard_Unarmed_Idle_com";
            };
            class rnt_gepard_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_gepard_Unarmed_Idle_com";
                stopRelaxed = "rnt_gepard_Unarmed_Idle_com";
                default = "rnt_gepard_Unarmed_Idle_com";
                Stand = "rnt_gepard_Unarmed_Idle_com";
                HandGunOn = "rnt_gepard_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_gepard_Unarmed_Idle_com";
                Binoculars = "rnt_gepard_Unarmed_Binoc_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
                civil = "rnt_gepard_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_gepard_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_gepard_Die_com";
                default = "rnt_gepard_Die_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
            };
            class rnt_gepard_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_gepard_Die_com";
                default = "rnt_gepard_Die_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
            };
            class rnt_gepard_Pistol_Actions_com : rnt_gepard_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_gepard_Pistol_com";
                stopRelaxed = "rnt_gepard_Pistol_com";
                default = "rnt_gepard_Pistol_com";
                Binoculars = "rnt_gepard_Pistol_Binoc_com";
                Stand = "rnt_gepard_Pistol_Idle_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_gepard_Binoc_Actions_com : rnt_gepard_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_gepard_Pistol_Binoc_com";
                stopRelaxed = "rnt_gepard_Pistol_Binoc_com";
                default = "rnt_gepard_Binoc_com";
            };
            class rnt_gepard_BinocPistol_Actions_com : rnt_gepard_Binoc_Actions_com 
            {
                stop = "rnt_gepard_Pistol_Binoc_com";
                stopRelaxed = "rnt_gepard_Pistol_Binoc_com";
                default = "rnt_gepard_Pistol_Binoc_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
            };
            class rnt_gepard_BinocUnarmed_Actions_com : rnt_gepard_Binoc_Actions_com 
            {
                stop = "rnt_gepard_Unarmed_Binoc_com";
                stopRelaxed = "rnt_gepard_Unarmed_Binoc_com";
                default = "rnt_gepard_Unarmed_Binoc_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
            };
            class rnt_gepard_Idle_Actions_com : rnt_gepard_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_gepard_Idle_com";
                stopRelaxed = "rnt_gepard_Idle_com";
                default = "rnt_gepard_Idle_com";
                Combat = "rnt_gepard_Unarmed_Idle_com";
                fireNotPossible = "rnt_gepard_Unarmed_Idle_com";
                PlayerStand = "rnt_gepard_Unarmed_Idle_com";
            };
            class rnt_gepard_IdlePistol_Actions_com : rnt_gepard_Action_Actions_com 
            {
                Stand = "rnt_gepard_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_gepard_Pistol_Idle_com";
                stopRelaxed = "rnt_gepard_Pistol_Idle_com";
                default = "rnt_gepard_Pistol_Idle_com";
                Combat = "rnt_gepard_Pistol_com";
                fireNotPossible = "rnt_gepard_Pistol_com";
                PlayerStand = "rnt_gepard_Pistol_com";
                die = "rnt_gepard_Die_com";
                Unconscious = "rnt_gepard_Die_com";
            };
        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            //Driver
            class Redd_Gepard_Driver: Crew
            {
                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Driver",1};
                ConnectTo[]={"KIA_Redd_Gepard_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 
            };
            class Redd_Gepard_Driver_Out: Crew
            {
                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 
            };
            class KIA_Redd_Gepard_Driver: DefaultDie
		    {
                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
		    };
            class KIA_Redd_Gepard_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
		    };

            //Commander
            class Redd_Gepard_Commander: Crew
            {
                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 
            };
            class Redd_Gepard_Commander_Out: Crew
            {
                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 
            };
            class Redd_Gepard_Commander_Out_high: Crew
            {
                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Commander_Out_high.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 
            };
            class KIA_Redd_Gepard_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Gepard_Commander_Out: DefaultDie
		    {
                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};
		    };
            
            //Gunner
            class Redd_Gepard_Gunner: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Gunner.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Gunner",1};
                ConnectTo[]={"KIA_Redd_Gepard_Gunner", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class Redd_Gepard_Gunner_Out: Crew
            {

                file = "\Redd_Tank_Gepard_1A2\anims\Redd_Gepard_Gunner_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Gepard_Gunner_Out",1};
                ConnectTo[]={"KIA_Redd_Gepard_Gunner_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            class KIA_Redd_Gepard_Gunner: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Gunner.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            class KIA_Redd_Gepard_Gunner_Out: DefaultDie
		    {

                file = "\Redd_Tank_Gepard_1A2\anims\KIA_Redd_Gepard_Gunner_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_gepard_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_gepard_Pistol_Actions_com";
                variantsAI[] = {"rnt_gepard_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_gepard_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_gepard_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_gepard_FromPistol_com", 0.1, "rnt_gepard_Pistol_Idle_com", 0.2, "rnt_gepard_Unarmed_Idle_com", 0.2, "rnt_gepard_Die_com", 0.5};
             };
            class rnt_gepard_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_gepard_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_gepard_FromPistol_com", 0.1, "rnt_gepard_Pistol_Idle_com", 0.2, "rnt_gepard_Unarmed_Idle_com", 0.2, "rnt_gepard_Die_com", 0.5};
            };
            class rnt_gepard_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_gepard_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_gepard_Pistol_com", 0.1, "rnt_gepard_FromPistol_com", 0.1, "rnt_gepard_Unarmed_Idle_com", 0.1, "rnt_gepard_Die_com", 0.1};
                variantsAI[] = {"rnt_gepard_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_gepard_Pistol_Idle_Idling_com", 1};
            };
            class rnt_gepard_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_gepard_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_gepard_Pistol_com", 0.1, "rnt_gepard_FromPistol_com", 0.1, "rnt_gepard_Unarmed_Idle_com", 0.1, "rnt_gepard_Die_com", 0.1};
            };
            class rnt_gepard_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_gepard_Pistol_Actions_com";
                ConnectTo[] = {"rnt_gepard_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_gepard_Pistol_Actions_com";
                ConnectTo[] = {"rnt_gepard_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_high.rtm";
                actions = "rnt_gepard_Pistol_Actions_com";
                ConnectTo[] = {"rnt_gepard_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_gepard_Action_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_gepard_FromBinoc_com", 0.1, "rnt_gepard_Die_com", 0.1};
            };
            class rnt_gepard_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_gepard_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_gepard_Pistol_FromBinoc_com", 0.1, "rnt_gepard_Die_com", 0.1};
            };
            class rnt_gepard_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_gepard_Action_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_gepard_Binoc_Actions_com";
                ConnectTo[] = {"rnt_gepard_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_gepard_Action_Actions_com";
                ConnectTo[] = {"rnt_gepard_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_gepard_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_gepard_FromPistol_End_com", 0.1, "rnt_gepard_ToPistol_End_com", 0.1, "rnt_gepard_Unarmed_ToBinoc_com", 0.1, "rnt_gepard_Die_com", 0.1};
                variantsAI[] = {"rnt_gepard_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_gepard_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_gepard_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_gepard_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_gepard_FromPistol_End_com", 0.1, "rnt_gepard_ToPistol_End_com", 0.1, "rnt_gepard_Unarmed_ToBinoc_com", 0.1, "rnt_gepard_Die_com", 0.1};
            };
            class rnt_gepard_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_gepard_Unarmed_FromBinoc_com", 0.1, "rnt_gepard_Die_com", 0.1};
            };
            class rnt_gepard_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\redd_tank_gepard_1a2\anims\Redd_Gepard_Commander_out_bino.rtm";
                actions = "rnt_gepard_Action_Actions_com";
                ConnectTo[] = {"rnt_gepard_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Die_com : vehicle_turnout_1_Die 
            {
                file = "\redd_tank_gepard_1a2\anims\KIA_Redd_Gepard_Commander_out.rtm";
                actions = "rnt_gepard_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_gepard_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\redd_tank_gepard_1a2\anims\KIA_Redd_Gepard_Commander_out.rtm";
                actions = "rnt_gepard_DeadPistol_Actions_com";
                showHandGun = 1;
            };
        };
    };