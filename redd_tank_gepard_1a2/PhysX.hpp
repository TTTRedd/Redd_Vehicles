	
	
	//Basic parameters
	simulation = tankX;
	dampersBumpCoef = 0.3;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 750 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 750;

	//Differential parameters
	//Nix

	//Engine parameters
	maxOmega = 261.8;
	minOmega = 52.36;
	enginePower = 619;
	peakTorque = 2860;
	engineMOI = 20;
	idleRpm = 500;
	redRpm = 2500;
	clutchStrength = 5;
	dampingRateFullThrottle = 0.25;
	dampingRateZeroThrottleClutchEngaged = 8;
	dampingRateZeroThrottleClutchDisengaged = 0.25;
	maxSpeed = 50;
	thrustDelay	= 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef = 0.62;
	slowSpeedForwardCoef = 0.12;

	tankTurnForce = 11*47500;
	tankTurnForceAngMinSpd = 0.5;
	tankTurnForceAngSpd = 0.5;

	accelAidForceCoef = 3;
	accelAidForceYOffset = -1;
	accelAidForceSpd = 20;
	
	torqueCurve[] = 
	{

		{"(0/2500)","(0/2860)"},
		{"(357/2500)","(2600/2860)"},
		{"(714/2500)","(2860/2860)"},
		{"(1071/2500)","(2860/2860)"},
		{"(1500/2500)","(2860/2860)"},
		{"(1785/2500)","(2860/2860)"},
		{"(2200/2500)","(2650/2860)"},
		{"(2500/2500)","(0/2860)"}

	};

	//Floating and sinking
	waterPPInVehicle = 0;
	maxFordingDepth = -0.75;
	waterResistance = 0;
	canFloat = 0;
	waterLeakiness = 10;
	
	//Anti-roll bars
	antiRollbarForceCoef = 85;
	antiRollbarForceLimit = 85;	
	antiRollbarSpeedMin = 10;
	antiRollbarSpeedMax	= 50;

	//Gearbox
	class complexGearbox 
	{
		
		GearboxRatios[] = {"R1", -2.3, "N", 0, "D1", 7, "D2", 3.2, "D3", 2, "D4", 1.6};
		TransmissionRatios[] = {"High",9};
		gearBoxMode        = "auto";
		moveOffGear        = 1;
		driveString        = "D";
		neutralString      = "N";
		reverseString      = "R";

	};

	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{

		1.0,0.45,
		0.6,0.45,
		0.95,0.45,
		0.95,0.45,
		0.95,0.45,
		1.0,0.75
		
	};

	switchTime = 0;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference = 2.64;
	numberPhysicalWheels = 18;
	turnCoef = 5;

	class Wheels
	{
		class L2
		{
			
			boneName = "wheel_podkoloL1";
			center   = "wheel_1_2_axis";
			boundary = "wheel_1_2_bound";
			damping = 75;
			steering = 0;
			side = "left";
			mass = 150;
			width = 0.51;
			MOI = 8;
			latStiffX = 2.5;
			latStiffY = 50;
			longitudinalStiffnessPerUnitGravity = 18000;
			maxBrakeTorque = 47500;
			sprungMass = 3393;
			springStrength = 143354;
			springDamperRate = 13233;
			dampingRate = 120;
			dampingRateInAir = 120;
			dampingRateDamaged = 10;
			dampingRateDestroyed = 10000;
			maxCompression = 0.15;
			maxDroop = 0.15;
			frictionVsSlipGraph[]={{0,0.35},{0.35,1},{1,0.35}};

		};
		class L3: L2
		{
			
			boneName="wheel_podkolol2";
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			
		};
		
		class L4: L2
		{
			
			boneName="wheel_podkolol3";
			center="wheel_1_4_axis";
			boundary="wheel_1_4_bound";
			
		};
		
		class L5: L2
		{
			
			boneName="wheel_podkolol4";
			center="wheel_1_5_axis";
			boundary="wheel_1_5_bound";
			
		};
		
		class L6: L2
		{
			
			boneName="wheel_podkolol5";
			center="wheel_1_6_axis";
			boundary="wheel_1_6_bound";
			
		};
		
		class L7: L2
		{
			
			boneName="wheel_podkolol6";
			center="wheel_1_7_axis";
			boundary="wheel_1_7_bound";
			
		};

		class L8: L2
		{
			
			boneName="wheel_podkolol7";
			center="wheel_1_8_axis";
			boundary="wheel_1_8_bound";
			
		};

		//Triebrad
		class L1: L2
		{
			
			boneName="";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			maxDroop=0;
			maxCompression=0;

		};

		//Umlenkrolle
		class L9: L2
		{
			
			boneName="";
			center="wheel_1_9_axis";
			boundary="wheel_1_9_bound";
			maxDroop=0;
			maxCompression=0;
			
		};
		
		class R2: L2
		{
			
			side="right";
			boneName="wheel_podkolop1";
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			
		};
		
		class R3: R2
		{
			
			boneName="wheel_podkolop2";
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			
		};
		class R4: R2
		{
			
			boneName="wheel_podkolop3";
			center="wheel_2_4_axis";
			boundary="wheel_2_4_bound";
			
		};
		
		class R5: R2
		{
			
			boneName="wheel_podkolop4";
			center="wheel_2_5_axis";
			boundary="wheel_2_5_bound";
			
		};
		
		class R6: R2
		{
			
			boneName="wheel_podkolop5";
			center="wheel_2_6_axis";
			boundary="wheel_2_6_bound";
			
		};
		
		class R7: R2
		{
			
			boneName="wheel_podkolop6";
			center="wheel_2_7_axis";
			boundary="wheel_2_7_bound";
			
		};

		class R8: R2
		{
			
			boneName="wheel_podkolop7";
			center="wheel_2_8_axis";
			boundary="wheel_2_8_bound";
			
		};

		//Triebrad
		class R1: R2
		{
			
			boneName="";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			maxDroop=0;
			maxCompression=0;
			
		};

		//Umlenkrolle
		class R9: R2
		{
			
			boneName="";
			center="wheel_2_9_axis";
			boundary="wheel_2_9_bound";
			maxDroop=0;
			maxCompression=0;

		};

	};