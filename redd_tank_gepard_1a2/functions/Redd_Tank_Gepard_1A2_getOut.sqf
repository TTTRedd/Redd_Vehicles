

	//triggered by BI eventhandler "getOut"
	
	params ["_veh","_pos","_unit","_turret"];

	//checks if gunner gets out
	if (_turret isEqualTo [0]) then
	{

		_veh setVariable ['Redd_Gepard_Gunner_Out', false,true];

	};

	//checks if commander gets out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Gepard_Commander_Out', false,true];

	};

	//check if unit is in bino turret
	if (_turret isEqualTo [1]) then
	{	
		
		_veh setVariable ['Redd_Gepard_Bino_In', false, true];
		_veh setVariable ['Redd_Gepard_Commander_Out', false,true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];

	};

	if (_veh getVariable 'has_camonet') then
	{
		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");

		_unit setUnitTrait ["camouflageCoef", _camouflage];
	};