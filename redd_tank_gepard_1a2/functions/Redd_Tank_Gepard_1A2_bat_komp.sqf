

	//Function for Eden attributes to set battalion and company number

	if (!isServer) exitWith {};

	params["_veh"];

	_b = _veh getVariable ["Redd_Tank_Gepard_1A2_Rgt_Btl",""];
	_k = _veh getVariable ["Redd_Tank_Gepard_1A2_Kompanie",""];

	//sets regiment or battalion
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_2_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_3_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_4_ca.paa"];
		
		};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_5_ca.paa"];
		
		};

		case ("6"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_6_ca.paa"];
		
		};

		case ("7"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_7_ca.paa"];
		
		};

		case ("8"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_8_ca.paa"];
		
		};

		case ("10"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_10_ca.paa"];
		
		};

		case ("11"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_11_ca.paa"];
		
		};

		case ("12"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_12_ca.paa"];
		
		};

		case ("13"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_13_ca.paa"];
		
		};

		case ("14"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_14_ca.paa"];
		
		};

		case ("111"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_111_ca.paa"];
		
		};

		case ("112"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_112_ca.paa"];
		
		};

		case ("131"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_131_ca.paa"];
		
		};

		case ("132"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_132_ca.paa"];
		
		};

		case ("141"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_141_ca.paa"];
		
		};

		case ("142"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_142_ca.paa"];
		
		};

		case ("620"):
		{
		
			_veh setObjectTextureGlobal [15, "\Redd_Vehicles_Main\data\Redd_Bataillon_620_ca.paa"];
		
		};

	};

	//sets company
	switch toLower (_k) do 
	{
	
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_1_ca.paa"];
				
		};

		case ("2"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_2_ca.paa"];
				
		};

		case ("3"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_3_ca.paa"];
				
		};

		case ("4"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_4_ca.paa"];
				
		};
		
	};