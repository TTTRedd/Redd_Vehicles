

	//triggered by BI eventhandler "GetIn"
	
	params ["_veh","_pos","_unit","_turret"];

	//checks if gunner gets in
	if (_turret isEqualTo [0]) then
	{

		if !(_veh getVariable ["Redd_Gepard_Commander_Out",false]) then
		{

			if (_veh animationSourcePhase "Turmluke_Rot_Source" == 1) then
			{

				_veh animateSource ["Turmluke_Rot_Source",0];

			};
		
		};

	};

	//checks if commander gets out
	if (_turret isEqualTo [0,0]) then
	{

		if !(_veh getVariable ["Redd_Gepard_Gunner_Out",false]) then
		{

			if (_veh animationSourcePhase "Turmluke_Rot_Source" == 1) then
			{

				_veh animateSource ["Turmluke_Rot_Source",0];

			};
		
		};

	};

	if (_veh getVariable 'has_camonet') then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
		_camouflage_new = _camouflage/100*80;
		_unit setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

	};