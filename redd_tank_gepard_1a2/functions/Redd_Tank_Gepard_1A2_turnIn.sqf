

	//triggered by BI eventhandler "turnIn"
	
	params ["_veh","_unit","_turret"];

	//checks if gunner turns out
	if (_turret isEqualTo [0]) then
	{

		_veh setVariable ['Redd_Gepard_Gunner_Out', false,true]; 
		
		if (!(_veh getVariable ["Redd_Gepard_Commander_Out",false]) or !(_veh getVariable ["Redd_Gepard_Bino_In",false])) then
		{

			_veh animateSource ["Turmluke_Rot_Source",0];

		};

	};

	//checks if gunner turns out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Gepard_Commander_Out', false,true];
		
		if !(_veh getVariable ["Redd_Gepard_Gunner_Out",false]) then
		{

			_veh animateSource ["Turmluke_Rot_Source",0];

		};

	};