
    
class CfgFunctions {
    class M120_Functions {

        tag = "rnt";

        class Functions {

            class m120CheckManReload {
                file = "\rnt_ace_compatibility\functions\rnt_m120CheckManReload.sqf";
            };

            class m120TypeReload {
                file = "\rnt_ace_compatibility\functions\rnt_m120TypeReload.sqf";
            };

            class m120ManReload {
                file = "\rnt_ace_compatibility\functions\rnt_m120ManReload.sqf";
            };

            class m120ManUnload {
                file = "\rnt_ace_compatibility\functions\rnt_m120ManUnload.sqf";
            };

            class m120_ACE_fired {
                file = "\rnt_ace_compatibility\functions\rnt_m120_ACE_fired.sqf";
            };
        };
    };
};