

//remove every magazine from weapon after each shot

params ["_veh"];

_mags = _veh magazinesTurret [0];
_mag = _mags select 0;
[_veh, [_mag,[0]]] remoteExec ["removeMagazinesTurret"];