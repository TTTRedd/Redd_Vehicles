

//Let the loader reload the weapon manually

params ["_veh","_unit","_mag"];

[5, [_veh,_unit,_mag],
{
	_veh = _this select 0 select 0;
	_unit = _this select 0 select 1;
	_mag = _this select 0 select 2;

	[_veh, [_mag,[0]]] remoteExec ["addMagazineTurret"];
	_unit removeMagazine _mag;
	
}, {}, "Reload"] call ace_common_fnc_progressBar;