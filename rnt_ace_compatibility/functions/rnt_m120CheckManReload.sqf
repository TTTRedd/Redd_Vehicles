	
	
// Check if player can reload mortar
	
params ["_veh","_unit"];

_canReload = false;

if ((alive _veh) and (count (_veh magazinesTurret [0]) == 0)) then
{
	_inventory = [_unit] call BIS_fnc_invString;

	if('Redd_1Rnd_120mm_Mo_shells' in _inventory || 'Redd_1Rnd_120mm_Mo_annz_shells' in _inventory || 'Redd_1Rnd_120mm_Mo_Flare_white' in _inventory || 'Redd_1Rnd_120mm_Mo_Smoke_white' in _inventory) then
	{
		_canReload = true;
	};
};

_canReload