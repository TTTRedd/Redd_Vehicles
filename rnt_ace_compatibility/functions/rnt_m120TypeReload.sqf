

// Check which type player can reload
		
params ["_veh","_unit","_mag"];

_canReload = false;

if ((alive _veh) and (count (_veh magazinesTurret [0]) == 0)) then
{
	_inventory = [_unit] call BIS_fnc_invString;

	if(_mag in _inventory) then
	{
		_canReload = true;
	};
};

_canReload