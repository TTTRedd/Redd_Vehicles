

//manual unload of mortar

params ["_veh","_unit"];

[5, [_veh,_unit],
{
	_veh = _this select 0 select 0;
	_unit = _this select 0 select 1;

	_mags = _veh magazinesTurret [0];
	_mag = _mags select 0;
	[_veh, [_mag,[0]]] remoteExec ["removeMagazinesTurret"];
	_unit addMagazine _mag;
	
}, {}, "Unload"] call ace_common_fnc_progressBar;