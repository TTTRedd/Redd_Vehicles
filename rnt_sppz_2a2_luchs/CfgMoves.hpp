

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {
            rnt_luchs_driver = "rnt_luchs_driver";
            rnt_luchs_driver_out = "rnt_luchs_driver_out";
            KIA_rnt_luchs_driver = "KIA_rnt_luchs_driver";
            KIA_rnt_luchs_driver_out = "KIA_rnt_luchs_driver_out";

            rnt_luchs_driver_2 = "rnt_luchs_driver_2";
            rnt_luchs_driver_2_out = "rnt_luchs_driver_2_out";
            KIA_rnt_luchs_driver_2 = "KIA_rnt_luchs_driver_2";
            KIA_rnt_luchs_driver_2_out = "KIA_rnt_luchs_driver_2_out";

            rnt_luchs_com = "rnt_luchs_com";
            rnt_luchs_com_out = "rnt_luchs_com_out";
            rnt_luchs_com_out_high = "rnt_luchs_com_out_high";
            rnt_luchs_com_out_mg = "rnt_luchs_com_out_mg";
            rnt_luchs_com_out_bino = "rnt_luchs_com_out_bino";
            KIA_rnt_luchs_com = "KIA_rnt_luchs_com";
            KIA_rnt_luchs_com_out = "KIA_rnt_luchs_com_out";
            KIA_rnt_luchs_com_out_mg = "KIA_rnt_luchs_com_out_mg";

            rnt_luchs_gun = "rnt_luchs_gun";
            rnt_luchs_gun_out = "rnt_luchs_gun_out";
            KIA_rnt_luchs_gun = "KIA_rnt_luchs_gun";
            KIA_rnt_luchs_gun_out = "KIA_rnt_luchs_gun_out"; 

            //ffv poses
            rnt_luchs_com_out_ffv = "rnt_luchs_Unarmed_Idle_com"; 
        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_luchs_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_luchs_Unarmed_Idle_com";
                stopRelaxed = "rnt_luchs_Unarmed_Idle_com";
                default = "rnt_luchs_Unarmed_Idle_com";
                Stand = "rnt_luchs_Unarmed_Idle_com";
                HandGunOn = "rnt_luchs_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_luchs_Unarmed_Idle_com";
                Binoculars = "rnt_luchs_Binoc_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
                civil = "rnt_luchs_Unarmed_Idle_com";
            };
            class rnt_luchs_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_luchs_Unarmed_Idle_com";
                stopRelaxed = "rnt_luchs_Unarmed_Idle_com";
                default = "rnt_luchs_Unarmed_Idle_com";
                Stand = "rnt_luchs_Unarmed_Idle_com";
                HandGunOn = "rnt_luchs_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_luchs_Unarmed_Idle_com";
                Binoculars = "rnt_luchs_Unarmed_Binoc_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
                civil = "rnt_luchs_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_luchs_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_luchs_Die_com";
                default = "rnt_luchs_Die_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
            };
            class rnt_luchs_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_luchs_Die_com";
                default = "rnt_luchs_Die_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
            };
            class rnt_luchs_Pistol_Actions_com : rnt_luchs_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_luchs_Pistol_com";
                stopRelaxed = "rnt_luchs_Pistol_com";
                default = "rnt_luchs_Pistol_com";
                Binoculars = "rnt_luchs_Pistol_Binoc_com";
                Stand = "rnt_luchs_Pistol_Idle_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_luchs_Binoc_Actions_com : rnt_luchs_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_luchs_Pistol_Binoc_com";
                stopRelaxed = "rnt_luchs_Pistol_Binoc_com";
                default = "rnt_luchs_Binoc_com";
            };
            class rnt_luchs_BinocPistol_Actions_com : rnt_luchs_Binoc_Actions_com 
            {
                stop = "rnt_luchs_Pistol_Binoc_com";
                stopRelaxed = "rnt_luchs_Pistol_Binoc_com";
                default = "rnt_luchs_Pistol_Binoc_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
            };
            class rnt_luchs_BinocUnarmed_Actions_com : rnt_luchs_Binoc_Actions_com 
            {
                stop = "rnt_luchs_Unarmed_Binoc_com";
                stopRelaxed = "rnt_luchs_Unarmed_Binoc_com";
                default = "rnt_luchs_Unarmed_Binoc_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
            };
            class rnt_luchs_Idle_Actions_com : rnt_luchs_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_luchs_Idle_com";
                stopRelaxed = "rnt_luchs_Idle_com";
                default = "rnt_luchs_Idle_com";
                Combat = "rnt_luchs_Unarmed_Idle_com";
                fireNotPossible = "rnt_luchs_Unarmed_Idle_com";
                PlayerStand = "rnt_luchs_Unarmed_Idle_com";
            };
            class rnt_luchs_IdlePistol_Actions_com : rnt_luchs_Action_Actions_com 
            {
                Stand = "rnt_luchs_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_luchs_Pistol_Idle_com";
                stopRelaxed = "rnt_luchs_Pistol_Idle_com";
                default = "rnt_luchs_Pistol_Idle_com";
                Combat = "rnt_luchs_Pistol_com";
                fireNotPossible = "rnt_luchs_Pistol_com";
                PlayerStand = "rnt_luchs_Pistol_com";
                die = "rnt_luchs_Die_com";
                Unconscious = "rnt_luchs_Die_com";
            };
        };
    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;
            
            //Driver
            class rnt_luchs_driver: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_driver.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_driver",1};
                ConnectTo[]={"KIA_rnt_luchs_driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class rnt_luchs_driver_out: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_driver_out.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_driver_out",1};
                ConnectTo[]={"KIA_rnt_luchs_driver_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_luchs_driver: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class KIA_rnt_luchs_driver_out: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_driver_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Driver2
            class rnt_luchs_driver_2: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_driver_2.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_driver_2",1};
                ConnectTo[]={"KIA_rnt_luchs_driver_2", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class rnt_luchs_driver_2_out: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_driver_2_out.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_driver_2_out",1};
                ConnectTo[]={"KIA_rnt_luchs_driver_2_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_rnt_luchs_driver_2: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_driver_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };
            
            class KIA_rnt_luchs_driver_2_out: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_driver_2_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //Commander
            class rnt_luchs_com: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_com",1};
                ConnectTo[]={"KIA_rnt_luchs_com", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class rnt_luchs_com_out: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out.rtm";
                interpolateTo[] = {};
                ConnectTo[]={"KIA_rnt_luchs_com_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class rnt_luchs_com_out_high: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_high.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_com_out",1};
                ConnectTo[]={"KIA_rnt_luchs_com_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class rnt_luchs_com_out_mg: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_mg.rtm";
                interpolateTo[] = {};
                ConnectTo[]={"KIA_rnt_luchs_com_out_mg", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_luchs_com: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_com.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class KIA_rnt_luchs_com_out: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_com_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class KIA_rnt_luchs_com_out_mg: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_com_out_mg.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class rnt_luchs_gun: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_gun.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_gun",1};
                ConnectTo[]={"KIA_rnt_luchs_gun", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class rnt_luchs_gun_out: Crew
            {

                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_gun_out.rtm";
                interpolateTo[] = {"KIA_rnt_luchs_gun_out",1};
                ConnectTo[]={"KIA_rnt_luchs_gun_out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };
            
            class KIA_rnt_luchs_gun: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_gun.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class KIA_rnt_luchs_gun_out: DefaultDie
		    {

                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_gun_out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_luchs_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_luchs_Pistol_Actions_com";
                variantsAI[] = {"rnt_luchs_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_luchs_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_luchs_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_luchs_FromPistol_com", 0.1, "rnt_luchs_Pistol_Idle_com", 0.2, "rnt_luchs_Unarmed_Idle_com", 0.2, "rnt_luchs_Die_com", 0.5};
             };
            class rnt_luchs_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_luchs_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_luchs_FromPistol_com", 0.1, "rnt_luchs_Pistol_Idle_com", 0.2, "rnt_luchs_Unarmed_Idle_com", 0.2, "rnt_luchs_Die_com", 0.5};
            };
            class rnt_luchs_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_luchs_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_luchs_Pistol_com", 0.1, "rnt_luchs_FromPistol_com", 0.1, "rnt_luchs_Unarmed_Idle_com", 0.1, "rnt_luchs_Die_com", 0.1};
                variantsAI[] = {"rnt_luchs_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_luchs_Pistol_Idle_Idling_com", 1};
            };
            class rnt_luchs_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_luchs_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_luchs_Pistol_com", 0.1, "rnt_luchs_FromPistol_com", 0.1, "rnt_luchs_Unarmed_Idle_com", 0.1, "rnt_luchs_Die_com", 0.1};
            };
            class rnt_luchs_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_luchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_luchs_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_luchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_luchs_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_high.rtm";
                actions = "rnt_luchs_Pistol_Actions_com";
                ConnectTo[] = {"rnt_luchs_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_luchs_Action_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_luchs_FromBinoc_com", 0.1, "rnt_luchs_Die_com", 0.1};
            };
            class rnt_luchs_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_luchs_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_luchs_Pistol_FromBinoc_com", 0.1, "rnt_luchs_Die_com", 0.1};
            };
            class rnt_luchs_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_luchs_Action_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_luchs_Binoc_Actions_com";
                ConnectTo[] = {"rnt_luchs_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_luchs_Action_Actions_com";
                ConnectTo[] = {"rnt_luchs_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_luchs_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_luchs_FromPistol_End_com", 0.1, "rnt_luchs_ToPistol_End_com", 0.1, "rnt_luchs_Unarmed_ToBinoc_com", 0.1, "rnt_luchs_Die_com", 0.1};
                variantsAI[] = {"rnt_luchs_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_luchs_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_luchs_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_luchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_luchs_FromPistol_End_com", 0.1, "rnt_luchs_ToPistol_End_com", 0.1, "rnt_luchs_Unarmed_ToBinoc_com", 0.1, "rnt_luchs_Die_com", 0.1};
            };
            class rnt_luchs_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_luchs_Unarmed_FromBinoc_com", 0.1, "rnt_luchs_Die_com", 0.1};
            };
            class rnt_luchs_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\rnt_sppz_2a2_luchs\anims\rnt_luchs_com_out_bino.rtm";
                actions = "rnt_luchs_Action_Actions_com";
                ConnectTo[] = {"rnt_luchs_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Die_com : vehicle_turnout_1_Die 
            {
                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_com_out.rtm";
                actions = "rnt_luchs_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_luchs_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\rnt_sppz_2a2_luchs\anims\KIA_rnt_luchs_com_out.rtm";
                actions = "rnt_luchs_DeadPistol_Actions_com";
                showHandGun = 1;
            };
        };
    };