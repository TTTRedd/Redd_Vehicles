	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: Animates mg3 rotation and moves commander to mg3
	//			 
	//	Example: n/a
	//				 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  
	//	Returns: true
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	

	params ["_vehicle","_unit"];

	if !(_vehicle getVariable 'Redd_Luchs_MG3_In') then
	{

		_vehicle setVariable ['Redd_Luchs_MG3_In', true, true];

		_vehicle animateSource ["fake_mg3_rot_source",1];

		waitUntil {_vehicle animationSourcePhase "fake_mg3_rot_source" == 1};

		_vehicle animateSource ["fake_mg3_hide_source",1];

		waitUntil {_vehicle animationSourcePhase "fake_mg3_hide_source" == 1};

		_vehicle animateSource ["mg3_hide_source",0];

		_unit action ['moveToTurret', _vehicle, [2]];
		[_vehicle,[[0,0],true]] remoteExecCall ['lockTurret'];

	}
	else
	{

		_unit action ['moveToTurret', _vehicle, [0,0]];
		[_vehicle,[[0,0],false]] remoteExecCall ['lockTurret'];
		_vehicle setVariable ['Redd_Luchs_MG3_In', false, true];
		_vehicle animateSource ["mg3_hide_source",1];

		waitUntil {_vehicle animationSourcePhase "mg3_hide_source" == 1};

		_vehicle animateSource ["fake_mg3_hide_source",0];
		_vehicle animateSource ["fake_mg3_rot_source",0];

	};

	true