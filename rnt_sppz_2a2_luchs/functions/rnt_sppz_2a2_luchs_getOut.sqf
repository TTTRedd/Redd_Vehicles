

	//Triggerd by BI eventhandler "getout"
	
	params ["_veh","_pos","_unit","_turret"];
	
	//check if unit is in mg3
	if (_turret isEqualTo [2]) then
	{	
		
		_veh setVariable ['Redd_Luchs_MG3_In', false, true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];

	};

	//check if unit is in bino turret
	if (_turret isEqualTo [3]) then
	{	
		
		_veh setVariable ['Redd_Luchs_Bino_In', false, true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];

	};

	if ((_veh getVariable 'has_camonet_large') or (_veh getVariable 'has_camonet')) then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");

		_unit setUnitTrait ["camouflageCoef", _camouflage];

	};