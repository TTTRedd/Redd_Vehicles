

	//Function for Eden attributes to set battalion and company number

	if (!isServer) exitWith {};

	params["_veh"];

	_b = _veh getVariable ["rnt_sppz_2a2_luchs_Bataillon",""];
	_k = _veh getVariable ["rnt_sppz_2a2_luchs_Kompanie",""];

	//sets regiment or battalion
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("5"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_5_ca.paa"];
				
		};

		case ("6"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_6_ca.paa"];
				
		};

		case ("7"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_7_ca.paa"];
				
		};

		case ("12"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_12_ca.paa"];
				
		};

		case ("13"):
		{
		
			_veh setObjectTextureGlobal [17, "\Redd_Vehicles_Main\data\Redd_Bataillon_13_ca.paa"];
				
		};
		
	};

	//sets company
	switch toLower (_k) do 
	{
	
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_1_ca.paa"];
				
		};

		case ("2"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_2_ca.paa"];
				
		};

		case ("3"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_3_ca.paa"];
				
		};

		case ("4"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_4_ca.paa"];
				
		};

		case ("5"):
		{
		
			_veh setObjectTextureGlobal [16, "\Redd_Vehicles_Main\data\Redd_Kompanie_5_ca.paa"];
				
		};
		
	};