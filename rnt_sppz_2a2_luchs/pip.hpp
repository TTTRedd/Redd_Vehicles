	
	
	class RenderTargets
	{
		
		//Driver left mirror
		class Mirror0
		{

			renderTarget = "rendertarget0";
			
			class CameraView1
			{

				pointPosition		= "pip_0_pos";
				pointDirection		= "pip_0_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
						
			}; 	

		};
		
		//Driver middle mirror
		class Mirror1
		{

			renderTarget = "rendertarget1";

			class CameraView1
			{

				pointPosition		= "pip_1_pos";
				pointDirection		= "pip_1_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver right mirror
		class Mirror2
		{

			renderTarget = "rendertarget2";

			class CameraView1
			{

				pointPosition		= "pip_2_pos";
				pointDirection		= "pip_2_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//commander
		class Mirror3
		{

			renderTarget = "rendertarget3"; 
			
			class CameraView1
			{

				pointPosition		= "pip_3_pos";
				pointDirection		= "pip_3_dir";
				renderQuality 		= 2;			
				renderVisionMode 	= 0;			
				fov 				= 0.7;	
						
			}; 	

		};
		
		//gunner
		class Mirror4
		{

			renderTarget = "rendertarget4";

			class CameraView1
			{

				pointPosition		= "pip_4_pos";
				pointDirection		= "pip_4_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	

			}; 

		};
		
		//left mirror
		class Mirror5
		{

			renderTarget = "rendertarget5";

			class CameraView1
			{

				pointPosition		= "pip_5_pos";
				pointDirection		= "pip_5_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};

		//right mirror
		class Mirror6
		{

			renderTarget = "rendertarget6";

			class CameraView1
			{

				pointPosition		= "pip_6_pos";
				pointDirection		= "pip_6_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};

		//reardriver mirror
		class Mirror7
		{

			renderTarget = "rendertarget7";

			class CameraView1
			{

				pointPosition		= "pip_7_pos";
				pointDirection		= "pip_7_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};
		
	};
