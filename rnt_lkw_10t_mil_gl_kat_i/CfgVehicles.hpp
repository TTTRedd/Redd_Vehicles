

class DefaultEventHandlers;
class WeaponFireGun;
class WeaponCloudsGun;
class WeaponFireMGun;
class WeaponCloudsMGun;

class CfgVehicles {

    class LandVehicle;
	class Car: LandVehicle
	{
		class NewTurret;
		class ViewOptics;
	};

	class Car_F: Car {

		class HitPoints;
		class Turrets;
	};

	class Truck_F: Car_F {

		class HitPoints: HitPoints {

			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			
			class HitBody;
			class HitFuel;
            class HitEngine;

			class HitLFWheel;
            class HitLF2Wheel;
            class HitLMWheel;
            class HitLBWheel;
            class HitRFWheel;
            class HitRF2Wheel;
            class HitRMWheel;
            class HitRBWheel;
		};

		class EventHandlers;
		class AnimationSources;

		class Turrets: Turrets {

			class MainTurret;
			class ViewGunner;
		};

		class ViewPilot;
	};

    class rnt_lkw_10t_mil_gl_kat_i_base: Truck_F {

        #include "Sounds.hpp"
        #include "PhysX.hpp"
        #include "Pip.hpp"

        side = 1;
        crew = "B_Soldier_F";
        author = "Tank, Redd";
        editorCategory = "Redd_Vehicles";
		editorSubcategory = "Redd_Trucks_2";
        getInAction = "GetInMRAP_01";
		getOutAction = "GetOutMedium";
        driverAction = "rnt_tonner_Driver";
        driverInAction="rnt_tonner_Driver";
        armor = 40;
        dustFrontLeftPos = "TrackFLL";
        dustFrontRightPos = "TrackFRR";
        dustBackLeftPos = "TrackBLL";
        dustBackRightPos = "TrackBRR";
        viewDriverInExternal = 1;
        enableManualFire = 0;
        driverCanSee = "31+32+14";
		driverCompartments = "Compartment1";
        aggregateReflectors[] = {{"Left","Right","Left_3","Right_3"},{"Left_2","Right_2"}};
        selectionBrakeLights = "zadni svetlo";
        selectionBackLights = "brzdove svetlo";
        driverLeftHandAnimName 	= "drivewheel";
        driverRightHandAnimName = "drivewheel";
        memoryPointSupply = "pos cargo";
        headGforceLeaningFactor[]={0.00075,0,0.0075};
        weapons[] = {"TruckHorn2"};
        wheelDamageThreshold = 0.49;
        wheelDestroyThreshold = 0.99;
        wheelDamageRadiusCoef = 0.85;
        wheelDestroyRadiusCoef = 0.55;
        destrType = "DestructWreck";
        memoryPointTrackFLL = "TrackFLL";
        memoryPointTrackFLR = "TrackFLR";
        memoryPointTrackFRL = "TrackFRL";
        memoryPointTrackFRR = "TrackFRR";
        memoryPointTrackBLL = "TrackBLL";
		memoryPointTrackBLR = "TrackBLR";
        memoryPointTrackBRL = "TrackBRL";
		memoryPointTrackBRR = "TrackBRR";
        threat[]={0.3,0.3,0.3};
        audible = 3;
        camouflage = 3;
        cost=50000;
        hideWeaponsDriver=1;
		hideWeaponsCargo=0;
        extCameraPosition[]={0,3,-11.5};
		camShakeCoef=0.80000001;

        class ViewPilot: ViewPilot {
			initAngleX=2;
		};

        ace_refuel_canReceive = 1;

        tf_hasLRradio=1;
        tf_isolatedAmount=0.25;

        //ACRE2
        class AcreRacks {
            class Rack_1 {
                displayName = "Zugkreis";
                componentName = ACRE_SEM90;
                allowedPositions[] = {"driver", "gunner", "commander"};
                disabledPositions[] = {};
                defaultComponents[] = {};
                mountedRadio = ACRE_SEM70;
                isRadioRemovable = 0;
                intercom[] = {};
            };
            
            class Rack_2 {
                displayName = "Kompaniekreis";
                componentName = ACRE_SEM90;
                allowedPositions[] = {"driver", "gunner", "commander"};
                disabledPositions[] = {};
                defaultComponents[] = {};
                mountedRadio = ACRE_SEM70;
                isRadioRemovable = 0;
                intercom[] = {};
            };
        };

        acre_hasInfantryPhone = 0;
		//Ende ACRE2

        hiddenSelections[] = {
            "kabine",
			"fahrgestell",
			"instr",
			"innenteile",
			"anbauteile",
			"kiste",
			"ladeflaeche",
			"ladung",

            "plate_1", //8
            "plate_2", //9
            "plate_3", //10
            "plate_4", //11
            "plate_5", //12
            "plate_6"  //13
        };

        class PlayerSteeringCoefficients{
            turnIncreaseConst = 0.1;
            turnIncreaseLinear = 1;
            turnIncreaseTime = 1;

            turnDecreaseConst = 1;
            turnDecreaseLinear = 1; 
            turnDecreaseTime = 0;

            maxTurnHundred = 0.7; 
        };

        // Damage textures
        class Damage {
            tex[] = {};
            mat[] = {
                "rnt_t_base\mats\rnt_t_base_anbauteile.rvmat",
                "rnt_t_base\mats\rnt_t_base_anbauteile_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_anbauteile_destruct.rvmat",

                "rnt_t_base\mats\rnt_t_base_fahrgestell.rvmat",
                "rnt_t_base\mats\rnt_t_base_fahrgestell_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_fahrgestell_destruct.rvmat", 

                "rnt_t_base\mats\rnt_t_base_kabine.rvmat",
                "rnt_t_base\mats\rnt_t_base_kabine_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_kabine_destruct.rvmat",

                "rnt_t_base\mats\rnt_t_base_kiste.rvmat",
                "rnt_t_base\mats\rnt_t_base_kiste_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_kiste_destruct.rvmat",

                "rnt_t_base\mats\rnt_t_base_glass.rvmat",
                "rnt_t_base\mats\rnt_t_base_glass_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_glass_destruct.rvmat",

                "rnt_t_base\mats\rnt_t_base_reflective_glass.rvmat",
                "rnt_t_base\mats\rnt_t_base_reflective_glass_damage.rvmat",
                "rnt_t_base\mats\rnt_t_base_reflective_glass_destruct.rvmat",

                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladeflaeche.rvmat",
                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladeflaeche_damage.rvmat",
                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladeflaeche_destruct.rvmat",

                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladung.rvmat",
                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladung_damage.rvmat",
                "rnt_lkw_10t_mil_gl_kat_i\mats\rnt_10t_ladung_destruct.rvmat"
            };
        };

        class Exhausts {

            class Exhaust1 {
                position = "exhaust1_pos"; 
                direction = "exhaust1_dir";
                effect = "ExhaustsEffect";
            };
        };

        class Reflectors {

            class Left {
                color[] = {2500, 1800, 1700};
                ambient[] = {3, 3, 3};
                position = "Light_L";
                direction = "Light_L_end";
                hitpoint = "Light_L";
                selection = "Light_L";
                size = 1;
                innerAngle = 75;
                outerAngle = 120;
                coneFadeCoef = 10;
                intensity = 1;
                useFlare = 0;
                dayLight = 1;
                flareSize = 1;

                class Attenuation {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 30;
                    hardLimitEnd = 60;
                };
            };

            class Right: Left {
                position = "Light_R";
                direction = "Light_R_end";
                hitpoint = "Light_R";
                selection = "Light_R";
            };

            class Left_2 {
                color[] = {2500, 1800, 1700};
                ambient[] = {0.25, 0.25, 0.25};
                position = "Light_L_2";
                direction = "Light_L_end_2";
                hitpoint = "Light_L";
                selection = "Light_L_2";
                size = 0.5;
                innerAngle = 0;
                outerAngle = 95;
                coneFadeCoef = 1;
                intensity = 0.05;
                useFlare = 0;
                dayLight = 1;
                flareSize = 0;

                class Attenuation {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 5;
                    hardLimitEnd = 10;
                };
            };

            class Right_2: Left_2 {
                position = "Light_R_2";
                direction = "Light_R_end_2";
                hitpoint = "Light_R";
                selection = "Light_R_2";
            };

            class Left_3 {
                color[] = {2500, 1800, 1700};
                ambient[] = {3, 3, 3};
                position = "Light_L_3";
                direction = "Light_L_end_3";
                hitpoint = "Light_L";
                selection = "Light_L";
                size = 1;
                innerAngle = 75;
                outerAngle = 120;
                coneFadeCoef = 1;
                intensity = 1;
                useFlare = 1;
                dayLight = 1;
                flareSize = 1;

                class Attenuation {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 30;
                    hardLimitEnd = 60;
                };
            };
            
            class Right_3: Left_3 {
                position = "Light_R_3";
                direction = "Light_R_end_3";
                hitpoint = "Light_R";
                selection = "Light_R";
            };
            
        };

        class HitPoints: HitPoints {
        
            class HitEngine: HitEngine {
                armor = 0.5;
                material = -1;
                name = "engine";
                visual = "damage_visual";
                passThrough = 0;
                minimalHit = 0.1;
                explosionShielding = 0.2;
                radius = 0.27;
                armorComponent="";
            };

            class HitFuel: HitFuel {
                armor = 0.5;
                material = -1;
                name = "fuel";
                visual = "damage_visual";
                passThrough = 0;
                minimalHit = 0.1;
                explosionShielding = 0.5;
                radius = 0.27;
                armorComponent="";
            };

            class HitBody: HitBody {
                armor = 2.0;
                material = -1;
                name = "karoserie";
                visual = "damage_visual";
                passThrough = 0;
                minimalHit = 0.1;
                explosionShielding = 1.5;
                radius = 0.22;
                armorComponent="";
            };

            class HitLFWheel: HitLFWheel {
                armor=0.5;
                material = -1;
                name = "wheel_1_1_steering";
                visual = "damage_LF_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitLF2Wheel: HitLF2Wheel {
                armor=0.5;
                material = -1;
                name = "wheel_1_2_steering";
                visual = "damage_LF2_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            }; 

            class HitLMWheel: HitLMWheel {
                armor=0.5;
                material = -1;
                name = "wheel_1_3_steering";
                visual = "damage_LFM_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            }; 

            class HitLBWheel: HitLBWheel {
                armor=0.5;
                material = -1;
                name = "wheel_1_4_steering";
                visual = "damage_LB_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitRFWheel: HitRFWheel {
                armor=0.5;
                material = -1;
                name = "wheel_2_1_steering";
                visual = "damage_RF_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitRF2Wheel: HitRF2Wheel {
                armor=0.5;
                material = -1;
                name = "wheel_2_2_steering";
                visual = "damage_RF2_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitRMWheel: HitRMWheel {
                armor=0.5;
                material = -1;
                name = "wheel_2_3_steering";
                visual = "damage_RFM_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitRBWheel: HitRBWheel {
                armor=0.5;
                material = -1;
                name = "wheel_2_4_steering";
                visual = "damage_RB_visual";
                minimalHit = 0.02;
                explosionShielding = 4;
                radius = 0.16;
            };

            class HitGlass1 {
                name = "glass1";
                visual = "glassF_left";
                explosionShielding = 1;
                armor = 0.75;
                passThrough = 0;
                radius = 0.05;
                material = -1;
                minimalHit = 0.01;
                armorComponent="";
			    hitpoint = "glass1";
            };

            class HitGlass2 {
                name = "glass2";
                visual = "glassF_right";
                explosionShielding = 1;
                armor = 0.75;
                passThrough = 0;
                radius = 0.05;
                material = -1;
                minimalHit = 0.01;
                armorComponent="";
            };

            class HitGlass3 {
                name = "glass3";
                visual = "glassFS";
                explosionShielding = 1;
                armor = 0.75;
                passThrough = 0;
                radius = 0.05;
                material = -1;
                minimalHit = 0.01;
                armorComponent="";
            };

            class HitGlass4 {
                name = "glass4";
                visual = "glassBS";
                explosionShielding = 1;
                armor = 0.75;
                passThrough = 0;
                radius = 0.05;
                material = -1;
                minimalHit = 0.01;
                armorComponent="";
            };
        };

        class TransportMagazines {};

        class TransportWeapons {};
        
        class TransportBackpacks {
            class _xx_B_AssaultPack_rgr{
                backpack = "B_AssaultPack_rgr";
                count = 1;
            };
        };
        
        class TransportItems {
            class _xx_Toolkit {
                name = "Toolkit";
                count = 1;
            };
        };
        
        class Turrets: Turrets {
        
            class MainTurret: NewTurret { //[0]
                weapons[] = {"Redd_MG3"};
                magazines[] = {
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120",
                    "Redd_Mg3_Mag_120"
                };

                gunnerName="STR_CoDriver_mg";
                proxyIndex = 1;
                gunnerInAction="rnt_tonner_CoDriver_mg";
                gunnerAction="rnt_tonner_CoDriver_mg";
                stabilizedInAxes = 3;
                viewGunnerInExternal = 1;
                hideProxyInCombat = 1;
                gunnerGetOutAction="GetOutMedium";
                commanding = 0;
                primaryGunner = 1;
                primaryObserver = 0;
                dontCreateAi = 1;
                gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                memoryPointGunnerOptics= "gunnerview2";
                 turretInfoType="Redd_RSC_MG3";
                discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
                discreteDistanceInitIndex = 4;
                gunnerRightHandAnimName = "RecoilHlaven";
                gunnerLeftHandAnimName = "RecoilHlaven";
                memoryPointsGetInGunner = "pos coDriver";
                memoryPointsGetInGunnerDir = "pos coDriver dir";
                canHideGunner = 0;
                forceHideGunner = 1;
                gunnerCompartments= "Compartment2";
                LODTurnedOut = 1000; //Gunner 1000
                lodTurnedIn = 1000; //Gunner 1000
                initElev = 0;
                minElev = -20;
                maxElev = 50;
                initTurn = 0;
                minTurn = -360;
                maxTurn = 360;
                disableSoundAttenuation = 1;
                outGunnerMayFire = 1;
                startEngine = 0;
                gunnerForceOptics = 0;

                class TurnIn {
                    limitsArrayTop[]= {

                        {50, -180},
                        {50, 180}
                    };

                    limitsArrayBottom[]= {
                        {-5, -180},
                        {-20, 0},
                        {-5, 180}
                    };
                };

                class ViewOptics: ViewOptics {
                    initFov = 0.4;
                    maxFov = 0.5;
                    minFov = 0.1;
                    visionMode[] = {"Normal","NVG"};
                };

                class OpticsOut {

                    class Wide {
                        initAngleX = 0;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.75;
                        maxFov = 1.25;
                        minFov = 0.25;
                        visionMode[] = {};
                        gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                        gunnerOpticsEffect[] = {};          
                    };
                };

                class OpticsIn {

                    class Wide {
                        initAngleX = 0;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.4;
                        maxFov = 0.4;
                        minFov = 0.4;
                        visionMode[] = {"Normal", "NVG"};
                        gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                        gunnerOpticsEffect[] = {};            
                    };
                    
                    class Narrow {
                        initAngleX = 0;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.2;
                        maxFov = 0.2;
                        minFov = 0.2;
                        visionMode[] = {"Normal", "NVG"};
                        gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
                        gunnerOpticsEffect[] = {};
                    };
                };
            };

            class CoDriver: NewTurret { // [1]
                body = "";
                gun = "";
                animationSourceBody = "";
                animationSourceGun = "";
                animationSourceHatch = "";
                enabledByAnimationSource = "";
                proxyType= "CPGunner";
                proxyIndex=2;
                gunnerName="$STR_Beifahrer";
                primaryGunner = 0;
                primaryObserver = 1;
                gunnerGetInAction="GetInMRAP_01";
                gunnerGetOutAction="GetOutMedium";
                gunnerForceOptics = 0;
                inGunnerMayFire = 0;
                viewGunnerInExternal = 1;
                gunnerCompartments= "Compartment1";
                LODTurnedIn = 1100; //Pilot 1100
                LODTurnedOut = 1100; //Pilot 1100
                startEngine = 0;
                dontCreateAi = 1;
                gunnerInAction="rnt_tonner_CoDriver";
                gunnerAction="rnt_tonner_CoDriver";
                memoryPointGunnerOptics = "";
                memoryPointsGetInGunner = "pos coDriver";
                memoryPointsGetInGunnerDir = "pos coDriver dir";
                soundAttenuationTurret = "TankAttenuation";
                disableSoundAttenuation = 1;
                hideWeaponsGunner = 1;

                class ViewGunner: ViewOptics{
					initFov = 0.9;
                    minFov = 0.25;
				    maxFov = 1.25;
                    initAngleX = 0;
                    minAngleX = -55;
                    maxAngleX = 85;
                    initAngleY = 0;
                    minAngleY = -150;
                    maxAngleY = 150;
				};

                class TurnIn {
                    limitsArrayTop[] = {
                        {45, -120}, 
                        {45, 120}
                    };

                    limitsArrayBottom[] = {
                        {-45, -120}, 
                        {-45, 120}
                    };
                };
            };
        };

        class AnimationSources {

            class TarnLichtHinten_Source {
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

            class TarnLichtVorne_Source {
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

            class LichterHide_Source {
                source = "user";
                initPhase = 0;
                animPeriod = 0;
            };

            class LichterHide_2_Source {
                source = "user";
                initPhase = 1;
                animPeriod = 0;
            };

            class HitLFWheel {
                source = "Hit";
                hitpoint = "HitLFWheel";
                raw = 1;
            };
            class HitLF2Wheel: HitLFWheel{hitpoint = "HitLF2Wheel";};
            class HitLMWheel: HitLFWheel{hitpoint = "HitLMWheel";};
            class HitLBWheel: HitLFWheel{hitpoint = "HitLBWheel";};
            class HitRFWheel: HitLFWheel{hitpoint = "HitRFWheel";};
            class HitRF2Wheel: HitLFWheel{hitpoint = "HitRF2Wheel";};
            class HitRMWheel: HitLFWheel{hitpoint = "HitRMWheel";};
            class HitRBWheel: HitLFWheel{hitpoint = "HitRBWheel";};

            class HitGlass1{
                source = "Hit";
                hitpoint = "HitGlass1";
            };
            class HitGlass2: HitGlass1 {hitpoint = "HitGlass2";};
            class HitGlass3: HitGlass1 {hitpoint = "HitGlass3";};
            class HitGlass4: HitGlass1 {hitpoint = "HitGlass4";};

            //mg_hatch
            class mg_hatch_source{
                source = user;
                initPhase = 0;
                animPeriod = 2;
            };

            //MG3
            class ReloadAnim{
                source="reload";
                weapon = "Redd_MG3";
            };
            class ReloadMagazine {
                source="reloadmagazine";
                weapon = "Redd_MG3";
            };
            class flash_mg3_source {
                source = "reload"; 
                weapon = "Redd_MG3";
                initPhase = 0;
            };

            //Glass
            class Glas_links_open_Source {
                source = "user";
                initPhase = 0;
                animPeriod = 3.5;
            };

            class Glas_rechts_open_Source {
                source = "user";
                initPhase = 0;
                animPeriod = 3.5;
            };

            //Türen
            class door_left_Source {
                source = user;
                initPhase = 0;
                animPeriod = 1.2;
            };

            class door_right_Source{
                source = user;
                initPhase = 0;
                animPeriod = 1.2;
            };

            class door_rear_Source{
                source = user;
                initPhase = 0;
                animPeriod = 1.2;
            };

            class mlc_15_hide_Source {	
                source = "user";
                initPhase = 1;
                animPeriod = 1;
            };
            class mlc_20_hide_Source {	
                source = "user";
                initPhase = 1;
                animPeriod = 1;
            };
            class mlc_25_hide_Source {	
                source = "user";
                initPhase = 0;
                animPeriod = 1;
            };
        };

        class UserActions {
            
            //Fenster
            class Glas_links_open {
                displayName = "$STR_Fenster_oeffnen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "player == (driver this) and (alive this) and (this animationSourcePhase 'Glas_links_open_Source') == 0";
                statement = "this animateSource ['Glas_links_open_Source',1]";
            };

            class Glas_rechts_open {
                displayName = "$STR_Fenster_oeffnen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [1] == player) and (alive this) and (this animationSourcePhase 'Glas_rechts_open_Source') == 0";
                statement = "this animateSource ['Glas_rechts_open_Source',1]";
            };

            class Glas_links_close {
                displayName = "$STR_Fenster_schliessen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "player == (driver this) and (alive this) and (this animationSourcePhase 'Glas_links_open_Source') == 1";
                statement = "this animateSource ['Glas_links_open_Source',0]";
            };

            class Glas_rechts_close {
                displayName = "$STR_Fenster_schliessen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [1] == player) and (alive this) and (this animationSourcePhase 'Glas_rechts_open_Source') == 1";
                statement = "this animateSource ['Glas_rechts_open_Source',0]";
            };

            //Türen
            class Driver_Door_Open {
                displayName = "$STR_Tuere_oeffnen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in [driver this]) and (this animationSourcePhase 'door_left_Source' == 0) and (alive this)";
                statement = "this animateSource ['door_left_Source', 1];";
            };

            class Driver_Door_Close {
                displayName = "$STR_Tuere_schliessen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in [driver this]) and (this animationSourcePhase 'door_left_Source' > 0) and (alive this)"; 
                statement = "this animateSource ['door_left_Source', 0];";
            };

            class Driver_Door_Open_ext {
                displayName = "$STR_Tuere_oeffnen";
                position = "door_open_links";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_left_Source' == 0) and (alive this)"; 
                statement = "this animateSource ['door_left_Source', 1];";
            };

            class Driver_Door_Close_ext {
                displayName = "$STR_Tuere_schliessen";
                position = "door_open_links";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_left_Source' > 0) and (alive this)";
                statement = "this animateSource ['door_left_Source', 0];";
            };

            class Co_Driver_Door_Open {
                displayName = "$STR_Tuere_oeffnen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [1] == player) and (this animationSourcePhase 'door_right_Source' == 0) and  (alive this)";
                statement = "this animateSource ['door_right_Source', 1];";
            };

            class Co_Driver_Door_Close {
                displayName = "$STR_Tuere_schliessen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [1] == player) and (this animationSourcePhase 'door_right_Source' > 0)  and (alive this)"; 
                statement = "this animateSource ['door_right_Source', 0];";
            };

            class Co_Driver_Door_Open_ext {
                displayName = "$STR_Tuere_oeffnen";
                position = "door_open_rechts";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_right_Source' == 0) and (alive this)";
                statement = "this animateSource ['door_right_Source', 1];";
            };

            class Co_Driver_Door_Close_ext {
                displayName = "$STR_Tuere_schliessen";
                position = "door_open_rechts";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_right_Source' > 0) and (alive this)"; 
                statement = "this animateSource ['door_right_Source', 0];";
            };

            class Rear_Door_Open {
                displayName = "$STR_HeckTuere_oeffnen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and !(player in [driver this]) and !(this turretUnit [0] == player) and !(this turretUnit [1] == player) and (alive this)";
                statement = "this animateSource ['door_rear_Source', 1];";
            };

            class Rear_Door_Close {
                displayName = "$STR_HeckTuere_schliessen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player in this) and (this animationSourcePhase 'door_rear_Source' > 0) and !(player in [driver this]) and !(this turretUnit [0] == player) and !(this turretUnit [1] == player)and (alive this)";
                statement = "this animateSource ['door_rear_Source', 0];";
            };

            class Rear_Door_Open_ext {
                displayName = "$STR_HeckTuere_oeffnen";
                position = "door_open_rear";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_rear_Source' == 0) and (alive this)";
                statement = "this animateSource ['door_rear_Source', 1];";
            };

            class Rear_Door_Close_ext {
                displayName = "$STR_HeckTuere_schliessen";
                position = "door_open_rear";
                radius = 2;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this animationSourcePhase 'door_rear_Source' > 0) and (alive this)"; 
                statement = "this animateSource ['door_rear_Source', 0];";
            };

            //Beleuchtung
            class TarnLichtHinten_ein {
                displayName = "$STR_Tarnbeleuchtung_hinten_ein";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";
            };
            
            class TarnLichtHinten_aus {
                displayName = "$STR_Tarnbeleuchtung_hinten_aus";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";
            };

            class TarnLichtVorne_ein {
                displayName = "$STR_Tarnbeleuchtung_vorne_ein";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];player action ['lightOn', this]";
            };
            
            class TarnLichtVorne_aus {
                displayName = "$STR_Tarnbeleuchtung_vorne_aus";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];";
            };

            class TarnLichtRundum_ein {
                displayName = "$STR_Tarnbeleuchtung_rundum_ein";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',1];this animateSource ['LichterHide_2_Source',0];this animateSource ['TarnLichtVorne_Source',0];this animateSource ['TarnLichtHinten_Source',0];player action ['lightOn', this]";
            };
            
            class TarnLichtRundum_aus {
                displayName = "$STR_Tarnbeleuchtung_rundum_aus";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
                statement = "this animateSource ['LichterHide_Source',0];this animateSource ['LichterHide_2_Source',1];this animateSource ['TarnLichtVorne_Source',1];this animateSource ['TarnLichtHinten_Source',1];";
            };

            //MG3
            class MG3_in {
                displayName = "$STR_MG3_benutzen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [1] == player) and (alive this)";
                statement = "[this,(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player])] spawn redd_fnc_t_base_move_to_mg3";
            };

            class MG3_out {
                displayName = "$STR_MG3_verlassen";
                position = "actionPoint";
                radius = 25;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(this turretUnit [0] == player) and (alive this)";
                statement = "[this,(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player])] spawn redd_fnc_t_base_move_to_mg3";
            };

            //Flaggen
            class Redd_removeflag {
                displayName = "$STR_Redd_flagge_entfernen";
                position = "actionPoint";
                radius = 5;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and (this getVariable 'has_flag')";
                statement = "[this,0] call Redd_fnc_t_base_flags";
            };

            class Redd_redFlag {
                displayName = "$STR_Redd_red_flagge_anbringen";
                position = "actionPoint";
                radius = 5;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and !(this getVariable 'has_flag')";
                statement = "[this,1] call Redd_fnc_t_base_flags";
            };

            class Redd_greenFlag {
                displayName = "$STR_Redd_green_flagge_anbringen";
                position = "actionPoint";
                radius = 5;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and !(this getVariable 'has_flag')";
                statement = "[this,2] call Redd_fnc_t_base_flags";
            };

            class Redd_blueFlag {
                displayName = "$STR_Redd_blue_flagge_anbringen";
                position = "actionPoint";
                radius = 5;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "!(player in this) and !(this getVariable 'has_flag')";
                statement = "[this,3] call Redd_fnc_t_base_flags";
            };

            class fixTurretBug {
                displayName = "Fix GetIn Bug"; 
                position = "actionPoint";
                radius = 5;
                onlyforplayer = 1;
                showWindow = 0;
                condition = "(alive this)";
                statement = "if (this turretUnit [0] isEqualTo objNull) then {[this,[[1],false]] remoteExecCall ['lockTurret']};";
            };
        };

        class EventHandlers: EventHandlers {
            init = "_this call redd_fnc_t_base_init";
            getOut = "_this call redd_fnc_t_base_getOut";
        };
    };

    //Transport
    class rnt_lkw_10t_mil_gl_kat_i_repair_fleck: rnt_lkw_10t_mil_gl_kat_i_base {
        scope=2;
        scopeCurator = 2;
        forceInGarage = 1;
        displayName = "$STR_rnt_lkw_10t_mil_gl_kat_i_repair_fleck";
        picture="\A3\Soft_F_EPC\Truck_03\Data\UI\truck_03_covered_CA.paa";
		icon="\A3\Soft_F_EPC\Truck_03\Data\UI\map_Truck_03_Covered_CA.paa";
        model = "\rnt_lkw_10t_mil_gl_kat_i\rnt_lkw_10t_mil_gl_kat_i";
        editorPreview="rnt_lkw_10t_mil_gl_kat_i\pictures\10t_fleck_repair.paa";
        transportRepair=1e+012;

        hiddenSelectionsTextures[] = {
            "rnt_t_base\data\rnt_t_base_kabine_blend_co",
            "rnt_t_base\data\rnt_t_base_fahrgestell_blend_co",
            "rnt_t_base\data\rnt_t_base_instr_blend_co",
            "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
            "rnt_t_base\data\rnt_t_base_anbauteile_blend_co",
            "rnt_t_base\data\rnt_t_base_kisten_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co"
        };

        // ace_repair_canRepair = 1;

        class textureSources {

            class Fleck {
                displayName = "$STR_rnt_lkw_10t_mil_gl_kat_i_repair_fleck";
                author = "RnT";
                factions[] = {"BLU_F"};
                textures[]= {
                    "rnt_t_base\data\rnt_t_base_kabine_blend_co",
                    "rnt_t_base\data\rnt_t_base_fahrgestell_blend_co",
                    "rnt_t_base\data\rnt_t_base_instr_blend_co",
                    "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
                    "rnt_t_base\data\rnt_t_base_anbauteile_blend_co",
                    "rnt_t_base\data\rnt_t_base_kisten_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co" 
                };
            };

            class Tropen {
                displayName = "$STR_rnt_lkw_10t_mil_gl_kat_i_repair_trope";
                author = "ReddNTank";
                factions[] = {"BLU_F"};
                textures[]= {
                    "rnt_t_base\data\rnt_t_base_kabine_trope_blend_co",
                    "rnt_t_base\data\rnt_t_base_fahrgestell_trope_blend_co",
                    "rnt_t_base\data\rnt_t_base_instr_blend_co",
                    "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
                    "rnt_t_base\data\rnt_t_base_anbauteile_trope_blend_co",
                    "rnt_t_base\data\rnt_t_base_kisten_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_trope_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co"
                };
            };

            class Winter {
                displayName = "$STR_rnt_lkw_10t_mil_gl_kat_i_repair_winter";
                author = "ReddNTank";
                factions[] = {"BLU_F"};
                textures[]= {
                    "rnt_t_base\data\rnt_t_base_kabine_winter_blend_co",
                    "rnt_t_base\data\rnt_t_base_fahrgestell_blend_co",
                    "rnt_t_base\data\rnt_t_base_instr_blend_co",
                    "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
                    "rnt_t_base\data\rnt_t_base_anbauteile_blend_co",
                    "rnt_t_base\data\rnt_t_base_kisten_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_winter_blend_co",
                    "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co"
                };
            };
        };

        textureList[]= {
            "Fleck", 1,
            "Tropen", 0,
            "Winter", 0
        };
    };

    class rnt_lkw_10t_mil_gl_kat_i_repair_trope: rnt_lkw_10t_mil_gl_kat_i_repair_fleck {
        scopeArsenal = 0;
        forceInGarage = 0;
        editorPreview="rnt_lkw_10t_mil_gl_kat_i\pictures\10t_trope_repair.paa";
        displayName="$STR_rnt_lkw_10t_mil_gl_kat_i_repair_trope";
        hiddenSelectionsTextures[] = {
            "rnt_t_base\data\rnt_t_base_kabine_trope_blend_co",
            "rnt_t_base\data\rnt_t_base_fahrgestell_trope_blend_co",
            "rnt_t_base\data\rnt_t_base_instr_blend_co",
            "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
            "rnt_t_base\data\rnt_t_base_anbauteile_trope_blend_co",
            "rnt_t_base\data\rnt_t_base_kisten_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_trope_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co"
        };
    };

    class rnt_lkw_10t_mil_gl_kat_i_repair_winter: rnt_lkw_10t_mil_gl_kat_i_repair_fleck {
        scopeArsenal = 0;
        forceInGarage = 0;
        editorPreview="rnt_lkw_10t_mil_gl_kat_i\pictures\10t_winter_repair.paa";
        displayName="$STR_rnt_lkw_10t_mil_gl_kat_i_repair_winter";
        hiddenSelectionsTextures[] = {
            "rnt_t_base\data\rnt_t_base_kabine_winter_blend_co",
            "rnt_t_base\data\rnt_t_base_fahrgestell_blend_co",
            "rnt_t_base\data\rnt_t_base_instr_blend_co",
            "rnt_t_base\data\rnt_t_base_innenteile_blend_co",
            "rnt_t_base\data\rnt_t_base_anbauteile_blend_co",
            "rnt_t_base\data\rnt_t_base_kisten_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladeflaeche_winter_blend_co",
            "rnt_lkw_10t_mil_gl_kat_i\data\rnt_10t_ladung_blend_co"
        };
    }; 
};