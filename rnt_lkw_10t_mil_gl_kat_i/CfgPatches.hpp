

class CfgPatches {
    class rnt_lkw_10t_mil_gl_kat_i {
        units[] = {
            "rnt_lkw_10t_mil_gl_kat_i_repair_fleck",
            "rnt_lkw_10t_mil_gl_kat_i_repair_trope",
            "rnt_lkw_10t_mil_gl_kat_i_repair_winter"
        };
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"A3_Soft_F","A3_Soft_F_Beta","Redd_Vehicles_Main","rnt_t_base"};
    };
};