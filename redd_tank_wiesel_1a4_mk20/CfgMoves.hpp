

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Wiesel_1A2_TOW_Driver = "Redd_Tank_Wiesel_1A2_TOW_Driver";
            Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            Redd_Tank_Wiesel_1A4_MK_Commander = "Redd_Tank_Wiesel_1A4_MK_Commander";
            Redd_Tank_Wiesel_1A4_MK_Commander_Out = "Redd_Tank_Wiesel_1A4_MK_Commander_Out";
            rnt_wieselMK20_com_out_high = "rnt_wieselMK20_com_out_high";
            rnt_wieselMK20_com_out_bino = "rnt_wieselMK20_com_out_bino";

            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            KIA_Redd_Tank_Wiesel_1A4_MK_Commander = "KIA_Redd_Tank_Wiesel_1A4_MK_Commander";
            KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out = "KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out";
            
            //ffv poses
            rnt_wieselMK20_com_out_ffv = "rnt_wieselMK20_Unarmed_Idle_com"; 
        };

        class Actions
        {
            class FFV_BaseActions;

            //com_out_ffv
            class rnt_wieselMK20_Action_Actions_com : FFV_BaseActions 
            {
                upDegree = "ManPosCombat";
                stop = "rnt_wieselMK20_Unarmed_Idle_com";
                stopRelaxed = "rnt_wieselMK20_Unarmed_Idle_com";
                default = "rnt_wieselMK20_Unarmed_Idle_com";
                Stand = "rnt_wieselMK20_Unarmed_Idle_com";
                HandGunOn = "rnt_wieselMK20_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_wieselMK20_Unarmed_Idle_com";
                Binoculars = "rnt_wieselMK20_Binoc_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
                civil = "rnt_wieselMK20_Unarmed_Idle_com";
            };
            class rnt_wieselMK20_IdleUnarmed_Actions_com : FFV_BaseActions
            {
                upDegree = "ManPosNoWeapon";
                stop = "rnt_wieselMK20_Unarmed_Idle_com";
                stopRelaxed = "rnt_wieselMK20_Unarmed_Idle_com";
                default = "rnt_wieselMK20_Unarmed_Idle_com";
                Stand = "rnt_wieselMK20_Unarmed_Idle_com";
                HandGunOn = "rnt_wieselMK20_Unarmed_Idle_com";
                PrimaryWeapon = "rnt_wieselMK20_Unarmed_Idle_com";
                Binoculars = "rnt_wieselMK20_Unarmed_Binoc_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
                civil = "rnt_wieselMK20_Unarmed_Idle_com";
                throwGrenade[] = {"GestureThrowGrenadeUna", "Gesture"};
            };
            class rnt_wieselMK20_Dead_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_wieselMK20_Die_com";
                default = "rnt_wieselMK20_Die_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
            };
            class rnt_wieselMK20_DeadPistol_Actions_com : FFV_BaseActions 
            {
                stop = "rnt_wieselMK20_Die_com";
                default = "rnt_wieselMK20_Die_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
            };
            class rnt_wieselMK20_Pistol_Actions_com : rnt_wieselMK20_Action_Actions_com 
            {
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselMK20_Pistol_com";
                stopRelaxed = "rnt_wieselMK20_Pistol_com";
                default = "rnt_wieselMK20_Pistol_com";
                Binoculars = "rnt_wieselMK20_Pistol_Binoc_com";
                Stand = "rnt_wieselMK20_Pistol_Idle_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
                throwGrenade[] = {"GestureThrowGrenadePistol", "Gesture"};
            };
            class rnt_wieselMK20_Binoc_Actions_com : rnt_wieselMK20_Action_Actions_com 
            {
                binocOn = "";
                upDegree = "ManPosBinocStand";
                stop = "rnt_wieselMK20_Pistol_Binoc_com";
                stopRelaxed = "rnt_wieselMK20_Pistol_Binoc_com";
                default = "rnt_wieselMK20_Binoc_com";
            };
            class rnt_wieselMK20_BinocPistol_Actions_com : rnt_wieselMK20_Binoc_Actions_com 
            {
                stop = "rnt_wieselMK20_Pistol_Binoc_com";
                stopRelaxed = "rnt_wieselMK20_Pistol_Binoc_com";
                default = "rnt_wieselMK20_Pistol_Binoc_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
            };
            class rnt_wieselMK20_BinocUnarmed_Actions_com : rnt_wieselMK20_Binoc_Actions_com 
            {
                stop = "rnt_wieselMK20_Unarmed_Binoc_com";
                stopRelaxed = "rnt_wieselMK20_Unarmed_Binoc_com";
                default = "rnt_wieselMK20_Unarmed_Binoc_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
            };
            class rnt_wieselMK20_Idle_Actions_com : rnt_wieselMK20_Action_Actions_com 
            {
                upDegree = "ManPosStand";
                stop = "rnt_wieselMK20_Idle_com";
                stopRelaxed = "rnt_wieselMK20_Idle_com";
                default = "rnt_wieselMK20_Idle_com";
                Combat = "rnt_wieselMK20_Unarmed_Idle_com";
                fireNotPossible = "rnt_wieselMK20_Unarmed_Idle_com";
                PlayerStand = "rnt_wieselMK20_Unarmed_Idle_com";
            };
            class rnt_wieselMK20_IdlePistol_Actions_com : rnt_wieselMK20_Action_Actions_com 
            {
                Stand = "rnt_wieselMK20_Pistol_Idle_com";
                upDegree = "ManPosHandGunStand";
                stop = "rnt_wieselMK20_Pistol_Idle_com";
                stopRelaxed = "rnt_wieselMK20_Pistol_Idle_com";
                default = "rnt_wieselMK20_Pistol_Idle_com";
                Combat = "rnt_wieselMK20_Pistol_com";
                fireNotPossible = "rnt_wieselMK20_Pistol_com";
                PlayerStand = "rnt_wieselMK20_Pistol_com";
                die = "rnt_wieselMK20_Die_com";
                Unconscious = "rnt_wieselMK20_Die_com";
            };
        };
    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Wiesel_1A2_TOW_Driver: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Driver_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A4_MK_Commander: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A4_MK_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A4_MK_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A4_MK_Commander_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class rnt_wieselMK20_com_out_high: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_high.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            //ffv part
            class vehicle_turnout_1_Aim_Pistol;
            class vehicle_turnout_1_Aim_Pistol_Idling;
            class vehicle_turnout_1_Idle_Pistol;
            class vehicle_turnout_1_Idle_Pistol_Idling;
            class vehicle_turnout_1_Aim_ToPistol;
            class vehicle_turnout_1_Aim_ToPistol_End ;
            class vehicle_turnout_1_Aim_FromPistol;
            class vehicle_turnout_1_Aim_FromPistol_End;
            class vehicle_turnout_1_Aim_Binoc;
            class vehicle_turnout_1_Aim_Pistol_Binoc;
            class vehicle_turnout_1_Aim_ToBinoc;
            class vehicle_turnout_1_Aim_ToBinoc_End;
            class vehicle_turnout_1_Aim_FromBinoc;
            class vehicle_turnout_1_Aim_FromBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc;
            class vehicle_turnout_1_Aim_Pistol_ToBinoc_End;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc;
            class vehicle_turnout_1_Aim_Pistol_FromBinoc_End;
            class vehicle_turnout_1_Idle_Unarmed;
            class vehicle_turnout_1_Idle_Unarmed_Idling;
            class vehicle_turnout_1_Aim_Unarmed_Binoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc;
            class vehicle_turnout_1_Aim_Unarmed_ToBinoc_End;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc;
            class vehicle_turnout_1_Aim_Unarmed_FromBinoc_End;
            class vehicle_turnout_1_Die;
            class vehicle_turnout_1_Die_Pistol;

            //com_out_ffv
            class rnt_wieselMK20_Pistol_com : vehicle_turnout_1_Aim_Pistol 
            {
                actions = "rnt_wieselMK20_Pistol_Actions_com";
                variantsAI[] = {"rnt_wieselMK20_Pistol_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselMK20_Pistol_Idling_com", 1};
                ConnectTo[] = {"rnt_wieselMK20_Pistol_ToBinoc_com", 0.1};
                InterpolateTo[] = {"rnt_wieselMK20_FromPistol_com", 0.1, "rnt_wieselMK20_Pistol_Idle_com", 0.2, "rnt_wieselMK20_Unarmed_Idle_com", 0.2, "rnt_wieselMK20_Die_com", 0.5};
             };
            class rnt_wieselMK20_Pistol_Idling_com : vehicle_turnout_1_Aim_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselMK20_Pistol_com", 0.1};
                InterpolateTo[] = {"rnt_wieselMK20_FromPistol_com", 0.1, "rnt_wieselMK20_Pistol_Idle_com", 0.2, "rnt_wieselMK20_Unarmed_Idle_com", 0.2, "rnt_wieselMK20_Die_com", 0.5};
            };
            class rnt_wieselMK20_Pistol_Idle_com : vehicle_turnout_1_Idle_Pistol 
            {
                actions = "rnt_wieselMK20_IdlePistol_Actions_com";
                InterpolateTo[] = {"rnt_wieselMK20_Pistol_com", 0.1, "rnt_wieselMK20_FromPistol_com", 0.1, "rnt_wieselMK20_Unarmed_Idle_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
                variantsAI[] = {"rnt_wieselMK20_Pistol_Idle_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselMK20_Pistol_Idle_Idling_com", 1};
            };
            class rnt_wieselMK20_Pistol_Idle_Idling_com : vehicle_turnout_1_Idle_Pistol_Idling 
            {
                ConnectTo[] = {"rnt_wieselMK20_Pistol_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_wieselMK20_Pistol_com", 0.1, "rnt_wieselMK20_FromPistol_com", 0.1, "rnt_wieselMK20_Unarmed_Idle_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
            };
            class rnt_wieselMK20_ToPistol_com : vehicle_turnout_1_Aim_ToPistol 
            {
                actions = "rnt_wieselMK20_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_ToPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_ToPistol_End_com : vehicle_turnout_1_Aim_ToPistol_End 
            {
                actions = "rnt_wieselMK20_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_FromPistol_com : vehicle_turnout_1_Aim_FromPistol 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_high.rtm";
                actions = "rnt_wieselMK20_Pistol_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_FromPistol_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_FromPistol_End_com : vehicle_turnout_1_Aim_FromPistol_End 
            {
                actions = "rnt_wieselMK20_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Binoc_com : vehicle_turnout_1_Aim_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                InterpolateTo[] = {"rnt_wieselMK20_FromBinoc_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
            };
            class rnt_wieselMK20_Pistol_Binoc_com : vehicle_turnout_1_Aim_Pistol_Binoc 
            {
                actions = "rnt_wieselMK20_BinocPistol_Actions_com";
                InterpolateTo[] = {"rnt_wieselMK20_Pistol_FromBinoc_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
            };
            class rnt_wieselMK20_ToBinoc_com : vehicle_turnout_1_Aim_ToBinoc 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_ToBinoc_End_com : vehicle_turnout_1_Aim_ToBinoc_End 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_FromBinoc_com : vehicle_turnout_1_Aim_FromBinoc 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_FromBinoc_End_com : vehicle_turnout_1_Aim_FromBinoc_End 
            {
                actions = "rnt_wieselMK20_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Pistol_ToBinoc_com : vehicle_turnout_1_Aim_Pistol_ToBinoc 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Pistol_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Pistol_ToBinoc_End_com : vehicle_turnout_1_Aim_Pistol_ToBinoc_End 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Pistol_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Pistol_FromBinoc_com : vehicle_turnout_1_Aim_Pistol_FromBinoc 
            {
                actions = "rnt_wieselMK20_Binoc_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Pistol_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Pistol_FromBinoc_End_com : vehicle_turnout_1_Aim_Pistol_FromBinoc_End 
            {
                actions = "rnt_wieselMK20_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Pistol_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Unarmed_Idle_com : vehicle_turnout_1_Idle_Unarmed 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_high.rtm";
                showWeaponAim = 0;
                actions = "rnt_wieselMK20_IdleUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_wieselMK20_FromPistol_End_com", 0.1, "rnt_wieselMK20_ToPistol_End_com", 0.1, "rnt_wieselMK20_Unarmed_ToBinoc_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
                variantsAI[] = {"rnt_wieselMK20_Idle_Unarmed_Idling_com", 1};
                variantsPlayer[] = {"rnt_wieselMK20_Idle_Unarmed_Idling_com", 1};
            };
            class rnt_wieselMK20_Idle_Unarmed_Idling_com : vehicle_turnout_1_Idle_Unarmed_Idling 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_high.rtm";
                showWeaponAim = 0;
                variantsPlayer[] = {};
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {"rnt_wieselMK20_FromPistol_End_com", 0.1, "rnt_wieselMK20_ToPistol_End_com", 0.1, "rnt_wieselMK20_Unarmed_ToBinoc_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
            };
            class rnt_wieselMK20_Unarmed_Binoc_com : vehicle_turnout_1_Aim_Unarmed_Binoc 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_BinocUnarmed_Actions_com";
                InterpolateTo[] = {"rnt_wieselMK20_Unarmed_FromBinoc_com", 0.1, "rnt_wieselMK20_Die_com", 0.1};
            };
            class rnt_wieselMK20_Unarmed_ToBinoc_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_ToBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Unarmed_ToBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_ToBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_Binoc_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Unarmed_FromBinoc_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_BinocUnarmed_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_FromBinoc_End_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Unarmed_FromBinoc_End_com : vehicle_turnout_1_Aim_Unarmed_FromBinoc_End 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\rnt_wieselMK20_com_out_bino.rtm";
                actions = "rnt_wieselMK20_Action_Actions_com";
                ConnectTo[] = {"rnt_wieselMK20_Unarmed_Idle_com", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Die_com : vehicle_turnout_1_Die 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                actions = "rnt_wieselMK20_Dead_Actions_com";
                ConnectTo[] = {"Unconscious", 0.1};
                InterpolateTo[] = {};
            };
            class rnt_wieselMK20_Die_Pistol_com : vehicle_turnout_1_Die_Pistol 
            {
                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                actions = "rnt_wieselMK20_DeadPistol_Actions_com";
                showHandGun = 1;
            };
        };
    };