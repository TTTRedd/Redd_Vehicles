

	//Wiesel init

	params["_veh"];

	[_veh] spawn redd_fnc_Wiesel_MK20_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Wiesel_MK20_Bat_Komp;//spawns function to set battalion and company numbers

	[_veh,[[1],true]] remoteExecCall ['lockTurret'];//Locks Bino turret

	_veh setVariable ['Redd_wieselMk20_Bino_In', false,true];
	_veh setVariable ['has_flag', false,true];//initiates varibale for flags
	_veh setVariable ['has_camonet', false,true];//initiates varibale for camonet
	_veh setVariable ['has_camonet_large', false,true];//initiates varibale for large camonet
