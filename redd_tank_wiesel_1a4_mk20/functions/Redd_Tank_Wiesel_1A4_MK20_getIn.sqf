

	//Triggerd by BI eventhandler "getout"
	
	params ["_veh","_pos","_unit","_turret"];

	//check if unit is in bino turret
	if (_turret isEqualTo [1]) then
	{	
		
		_veh setVariable ['Redd_wieselMk20_Bino_In', false, true];
		[_veh,[[0],false]] remoteExecCall ['lockTurret'];

	};

	if (_veh getVariable 'has_camonet_large') then
	{

		_unit setUnitTrait ["camouflageCoef", 0.1];

	};

	if (_veh getVariable 'has_camonet') then
	{

		_unit_class = typeOf _unit;
		_camouflage = getNumber (configFile >> "CfgVehicles" >> _unit_class >> "camouflage");
		_camouflage_new = _camouflage/100*80;
		_unit setUnitTrait ["camouflageCoef", _camouflage_new]; //80%

	};