

class CfgPatches{
	class rnt_mg3_static{
        units[] = {
			"rnt_mg3_static",
			"rnt_mg3_static_ai",
			"Item_rnt_mg3_kasten_fake",
			"rnt_mg3_kasten_fake"
		};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Static_F","Redd_Vehicles_Main"};
    };
};