

class CfgFunctions {

    class Redd {
        tag = "Redd";

        class Functions {
            
            class mg3_init {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_init.sqf";
            };

            class mg3_fired {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_fired.sqf";
            };

            class mg3_adjust_height {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_adjust_height.sqf";
            };

            class mg3_getIn {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_getIn.sqf";
            };

            class mg3_turn {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_turn.sqf";
            };

            class mg3_reload {
                file = "\rnt_mg3_static\functions\rnt_mg3_static_reload.sqf";
            };
        };
    };
};