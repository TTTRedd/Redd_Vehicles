

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Adjust height for MG3
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh","_upDown"];

_veh setVariable ["isInAnimation",true,true];

if (_veh getVariable ["middle",false] && (_upDown == "up")) then {
	_veh setVariable ["middle",false,true];
	_veh setVariable ["up",true,true];
	_veh animateSource ["ammo_belt_5_c_hide_sorce",1];
	_veh animateSource ["ammo_belt_5_s_hide_sorce",0];
	_veh animateSource ["ammo_belt_6_hide_sorce",0];
	_veh animateSource ["ammo_belt_7_hide_sorce",0];
	_veh animateSource ['mg3_up_source', 1];
	_veh animateSource ['sandsack_hide_Source', 0];
	waitUntil {_veh animationSourcePhase "mg3_up_source" == 1};
	if (!isNull gunner _veh) then {
		[gunner _veh, "rnt_mg3_g_high"] remoteExec ["switchMove", 0];
	};
} else { 
	if (_veh getVariable ["up",false] && (_upDown == "down")) then {
		_veh setVariable ["middle",true,true];
		_veh setVariable ["up",false,true];
		_veh animateSource ['mg3_up_source', 0];
		waitUntil {_veh animationSourcePhase 'mg3_up_source' == 0};
		_veh animateSource ["ammo_belt_5_c_hide_sorce",0];
		_veh animateSource ["ammo_belt_5_s_hide_sorce",1];
		_veh animateSource ["ammo_belt_6_hide_sorce",1];
		_veh animateSource ["ammo_belt_7_hide_sorce",1];
		_veh animateSource ['sandsack_hide_Source', 1];
		if (!isNull gunner _veh) then {
			[gunner _veh, "rnt_mg3_g"] remoteExec ["switchMove", 0];
		};
		_veh animateSource ['sandsack_hide_Source', 1];
	} else {
		if (_veh getVariable ["middle",false] && (_upDown == "down")) then{
			_veh setVariable ["middle",false,true];
			_veh setVariable ["down",true,true];
			_veh animateSource ['mg3_down_source', 1];
			waitUntil {_veh animationSourcePhase "mg3_down_source" == 1};
			_veh animateSource ["ammo_belt_3_s_hide_sorce",1];
			_veh animateSource ["ammo_belt_3_c_hide_sorce",0];
			_veh animateSource ["ammo_belt_4_hide_sorce",1];
			_veh animateSource ["ammo_belt_5_c_hide_sorce",1];
			if (!isNull gunner _veh) then {
				[gunner _veh, "rnt_mg3_g_low"] remoteExec ["switchMove", 0];
			};
		} else {
			if (_veh getVariable ["down",false] && (_upDown == "up")) then {
				_veh setVariable ["middle",true,true];
				_veh setVariable ["down",false,true];
				_veh animateSource ["ammo_belt_3_c_hide_sorce",1];
				_veh animateSource ["ammo_belt_3_s_hide_sorce",0];
				_veh animateSource ["ammo_belt_4_hide_sorce",0];
				_veh animateSource ["ammo_belt_5_c_hide_sorce",0];
				_veh animateSource ['mg3_down_source', 0];
				waitUntil {_veh animationSourcePhase 'mg3_down_source' == 0};
				if (!isNull gunner _veh) then {
					[gunner _veh, "rnt_mg3_g"] remoteExec ["switchMove", 0];
				};
			};
		};
	};
};

_veh setVariable ["isInAnimation",false,true];