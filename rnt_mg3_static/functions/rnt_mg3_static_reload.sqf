

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: Reloads the MG3
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_target","_caller","_redd_fake_belt","_redd_belt"];

if (_redd_fake_belt in backpackItems _caller) then {
	_MagArrayWeapon = magazinesAmmo _target;
	if (count _MagArrayWeapon == 0) then {
		[_target, _redd_belt] remoteExec ["addMagazine"];
		[_target] remoteExec ["reload"];
		_caller removeItem _redd_fake_belt;
	} else {
		_ammoCountWeapon = _MagArrayWeapon select 0 select 1;
		if (_ammoCountWeapon == 0) then {
			[_target, _redd_belt] remoteExec ["addMagazine"];
			[_target] remoteExec ["reload"];
			_caller removeItem _redd_fake_belt;
		};
	};
};