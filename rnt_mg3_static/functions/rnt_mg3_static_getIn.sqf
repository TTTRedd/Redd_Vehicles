

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Author: Redd
//
//	Description: GetIn for MG3
//			 
//	Example:
//						 		 
//	Parameter(s): 0: OBJECT - Vehicle
//				  
//	Returns: N/A
//  
///////////////////////////////////////////////////////////////////////////////////////////////////	

params ["_veh","_pos","_unit","_turret"];

if (_veh getVariable ["up",false]) then {
	[_unit, "rnt_mg3_g_high"] remoteExec ["switchMove", 0];
};

if (_veh getVariable ["down",false]) then {
	[_unit, "rnt_mg3_g_low"] remoteExec ["switchMove", 0];
};