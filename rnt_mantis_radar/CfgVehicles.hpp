class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class DefaultVehicleSystemsDisplayManagerLeft {
	class components;
};
class DefaultVehicleSystemsDisplayManagerRight {
	class components;
};
class VehicleSystemsTemplateLeftPilot: DefaultVehicleSystemsDisplayManagerLeft {
	class components;
};
class VehicleSystemsTemplateRightPilot: DefaultVehicleSystemsDisplayManagerRight {
	class components;
};
class Eventhandlers;

class CfgVehicles {

	class NonStrategic;
	class StaticShip;
	class Building;
	class House_F;
	class FloatingStructure_F;
	class thingx;
	class Ship;
	class LandVehicle;

	class StaticWeapon: LandVehicle {
		class Turrets;
		class HitPoints;
	};

	class StaticMGWeapon: StaticWeapon {
		class Turrets: Turrets{
			class MainTurret;
		};
		class Components;
		class UserActions;
	};

	class rnt_mantis_radar: StaticMGWeapon {
        model="\rnt_mantis_radar\rnt_mantis_radar";
        editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Static";
        displayName="Mantis Sensor";
		scope=2;
		scopeCurator=2;
        side=1;
        crew="B_UAV_AI";
		typicalCargo[]= {
			"B_UAV_AI"
		};
		author="RnT";
		picture="\A3\Static_F_Sams\Radar_System_01\Data\UI\Radar_System_01_picture_CA.paa";
		uiPicture="\A3\Static_F_Sams\Radar_System_01\Data\UI\Radar_System_01_picture_CA.paa";
		icon="\A3\Static_F_Sams\Radar_System_01\Data\UI\Radar_System_01_icon_CA.paa";
		hasDriver=0;
		hasGunner=1;
		isUav=1;
		uavCameraGunnerPos="pos_gunner_view";
		uavCameraGunnerDir="pos_gunner_view_dir";
        threat[]={0.1,0.1,1};
		cost=3000000;
        accuracy=0.12;
        editorPreview="rnt_mantis_radar\pictures\mantis_radar.paa";
		unitInfoType="RscUnitInfoStaticNoWeapon";
		
		hiddenSelections[]= {
			"radar_base",
            "radar_radar"
		};
		hiddenSelectionsTextures[]= {
			"rnt_mantis_radar\data\rnt_mantis_radar_base_blend_co",
            "rnt_mantis_radar\data\rnt_mantis_radar_radar_blend_co"		
		};
		
		extCameraPosition[]={0,1.5,-10};
        enableGPS=1;
		radartype=2;
		radarTarget=1;
		radarTargetSize=1;
		visualTarget=1;
		visualTargetSize=1.2;
		irTargetSize=0.5;
		reportRemoteTargets=1;
		receiveRemoteTargets=1;
		reportOwnPosition=1;
		lockDetectionSystem="CM_none";
		incomingMissileDetectionSystem="CM_Missile";
        animated=1;
        armor=80;
        armorStructural=2;
		damageResistance=0.0040000002;
		damageEffect="AirDestructionEffects";

		nameSound="veh_vehicle_truck_s";
        canFloat=0;
		waterLeakiness=100;
		waterResistanceCoef=0.039999999;
		maxFordingDepth=0.001;
		
		class Sounds {
			class engine {
				sound[]= {
					"A3\Sounds_F\vehicles\soft\Offroad_01\engine_ext_idle",
					0.31622776,
					1,
					100
				};
				frequency=1;
				volume=1;
			};
		};

        class Components: Components {
            class SensorsManagerComponent {
                class Components {
                    class IRSensorComponent: SensorTemplateIR {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=4000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=3500;
                            objectDistanceLimitCoef=1;
                            viewDistanceLimitCoef=1;
                        };
                        typeRecognitionDistance=3500;
                        maxTrackableSpeed=600;
                        angleRangeHorizontal=60;
                        angleRangeVertical=40;
                        animDirection="mainGun";
                        aimDown=-0.5;
                    };
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar {
                        class AirTarget {
                            minRange=10000;
                            maxRange=10000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget {
                            minRange=7000;
                            maxRange=7000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        typeRecognitionDistance=7000;
                        angleRangeHorizontal=360;
                        angleRangeVertical=100;
                        aimDown=-45;
                        maxTrackableSpeed=1388.89;
                    };
                    class DataLinkSensorComponent: SensorTemplateDataLink{};
                };
            };
        };
		
		class AnimationSources {
			class Hit_Hull {
				source = "Hit";
				hitpoint = "hit_hull";
				raw = 1;
			};
		};
		
		class Hitpoints: HitPoints {
			class Hit_Hull
			{
				armor=3;
				name="hit_hull";
				visual="damage_visual";
				radius=0.25;
				minimalHit=0.050000001;
				explosionShielding=1.5;
				depends="Total";
				passThrough=0.2;
				material=-1;
			};
		};

		class Damage {
			tex[]={};
			mat[]= {
				"rnt_mantis_radar\mats\rnt_mantis_radar_base.rvmat",
				"rnt_mantis_radar\mats\rnt_mantis_radar_base_damage.rvmat",
				"rnt_mantis_radar\mats\rnt_mantis_radar_base_destruct.rvmat",

				"rnt_mantis_radar\mats\rnt_mantis_radar_radar.rvmat",
				"rnt_mantis_radar\mats\rnt_mantis_radar_radar_damage.rvmat",
				"rnt_mantis_radar\mats\rnt_mantis_radar_radar_destruct.rvmat",
			};
		};

		class Turrets: Turrets {
			class MainTurret: MainTurret {
				minelev=-10;
				maxelev=75;
				minturn=-180;
				maxturn=180;
				initElev=15;
				initTurn=0;
				maxHorizontalRotSpeed=2.7;
				maxVerticalRotSpeed=2.7;
				soundServo[]=
				{
					"A3\Sounds_F\vehicles\armor\noises\servo_best",
					1.4125376,
					1,
					40
				};
				hasGunner=1;
				canUseScanners=1;
				gunnerName="mantis_radar_gunner";
				primary=1;
				primaryGunner=1;
				startEngine=0;
				enableManualFire=1;
				turretinfotype="RscOptics_Radar_02";
				forceHideGunner=1;
				gunnerforceoptics=1;
				gunnerOutForceOptics=1;
				viewgunnerinExternal=0;
				gunnerOpticsShowCursor=0;
				gunnerOutOpticsShowCursor=0;
				outGunnerMayFire=1;
				inGunnerMayFire=1;
				castGunnerShadow=0;
				showAllTargets=2;
				body="MainTurret";
				gun="MainGun";
				animationSourceBody="MainTurret";
				animationSourceGun="MainGun";
				uavCameraGunnerPos="pos_gunner_view";
				uavCameraGunnerDir="pos_gunner_view_dir";
				memoryPointGunnerOptics="pos_gunner_view";
				selectionFireAnim="zasleh";
				missileBeg="pos_missile";
				missileEnd="pos_missile_end";
				gunnerlefthandanimname="";
				gunnerrighthandanimname="";
				weapons[]= {
					"FakeWeapon"
				};
				magazines[]={};
				optics=1;
				gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";

				class OpticsIn {
					class Wide {
						opticsDisplayName="W";
						initAngleX=0;
						minAngleX=-30;
						maxAngleX=30;
						initAngleY=0;
						minAngleY=-100;
						maxAngleY=100;
						initFov=0.46599999;
						minFov=0.46599999;
						maxFov=0.46599999;
						visionMode[]= {
							"Normal",
							"NVG",
							"Ti"
						};
						thermalMode[]={0,1};
						gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
					};

					class Medium: Wide {
						opticsDisplayName="M";
						initFov=0.093000002;
						minFov=0.093000002;
						maxFov=0.093000002;
						gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_m_F";
					};

					class Narrow: Wide {
						opticsDisplayName="N";
						gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Gunner_AAA_01_n_F";
						initFov=0.028999999;
						minFov=0.028999999;
						maxFov=0.028999999;
					};
				};

				class Components: Components {
					class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft {
						class Components {
							class EmptyDisplay {
								componentType="EmptyDisplayComponent";
							};

							class MinimapDisplay {
								componentType="MinimapDisplayComponent";
								resource="RscCustomInfoMiniMap";
							};

							class UAVDisplay {
								componentType="UAVFeedDisplayComponent";
							};

							class SensorDisplay {
								componentType="SensorsDisplayComponent";
								range[]={16000,8000,4000,2000};
								resource="RscCustomInfoSensors";
							};
						};
					};

					class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight {
						defaultDisplay="SensorDisplay";
						class Components {
							class EmptyDisplay {
								componentType="EmptyDisplayComponent";
							};

							class MinimapDisplay {
								componentType="MinimapDisplayComponent";
								resource="RscCustomInfoMiniMap";
							};

							class UAVDisplay {
								componentType="UAVFeedDisplayComponent";
							};

							class SensorDisplay {
								componentType="SensorsDisplayComponent";
								range[]={16000,8000,4000,2000};
								resource="RscCustomInfoSensors";
							};
						};
					};
				};

				class HitTurret
				{
					armor=0.30000001;
					name="vez";
					visual="damage_visual";
					radius=0.25;
					passThrough=0;
					minimalHit=0.1;
					explosionShielding=1.2;
					material=-1;

					class DestructionEffects {
						class Smoke {
							simulation="particles";
							type="WeaponWreckSmoke";
							position="turretdestruct_pos";
							intensity=1;
							interval=1;
							lifeTime=5;
						};
					};
				};
			};
		};

		class AttributeValues {
			RadarUsageAI=1;
		};

		class UserActions: UserActions {
			delete PressXToFlipTheThing;
		};
	};
};